<?php require_once('conexion.php');?>
<?php require_once('consultar.php');?>
<?php
$cedula = $_GET['usr_cc'];

$qmenu = "SELECT  /* + ORDERED(S) */  COUNT(S.CEDULARE) TIENE
             FROM SOL_RETIROS_WEB S
            WHERE S.CEDULARE = $cedula
              AND S.VIGENTE IN (1)";
$sme= $conn->Execute($qmenu);
//$row = oci_execute($sme);
//if($row = oci_fetch_array($sme, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$sme->EOF){
   $vcontso = $sme->fields['TIENE'];
}

$qmaildiv = "SELECT (SELECT /* + INDEX(E EMPL_UK_IDX) */ E.EMAIL_INSTITUCIONAL
               FROM EMPLEADOS E
                   ,CARGOS C
              WHERE E.CARG_CARGO = C.CARGO
                AND E.CARG_FUERZA = C.FUERZA
                AND C.CARGO = 1903
                AND E.UNDE_CONSECUTIVO_LABORANDO = 5900
                AND E.ACTIVO = 'SI')||','||(SELECT /* + INDEX(E EMPL_UK_IDX) */ E.EMAIL_INSTITUCIONAL
               FROM EMPLEADOS E
                   ,CARGOS C
              WHERE E.CARG_CARGO = C.CARGO
                AND E.CARG_FUERZA = C.FUERZA
                AND C.CARGO IN (1227)
                AND E.UNDE_CONSECUTIVO_LABORANDO = 1900
                AND E.ACTIVO = 'SI')||','||(SELECT /* + INDEX(E EMPL_UK_IDX) */ E.EMAIL_INSTITUCIONAL
               FROM EMPLEADOS E
                   ,CARGOS C
              WHERE E.CARG_CARGO = C.CARGO
                AND E.CARG_FUERZA = C.FUERZA
                AND C.CARGO IN (1903)
                AND E.UNDE_CONSECUTIVO_LABORANDO = 50464
                AND E.ACTIVO = 'SI')||','||(SELECT /* + INDEX(E EMPL_UK_IDX) */ E.EMAIL_INSTITUCIONAL
               FROM EMPLEADOS E
                   ,CARGOS C
              WHERE E.CARG_CARGO = C.CARGO
                AND E.CARG_FUERZA = C.FUERZA
                AND C.CARGO IN (100410)
                AND E.UNDE_CONSECUTIVO_LABORANDO = 1600
                AND E.ACTIVO = 'SI') MAILDIV FROM DUAL";
$smadiv = $conn->Execute($qmaildiv);
//$row = oci_execute($smadiv);
//if($row = oci_fetch_array($smadiv, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$smadiv->EOF){
   $vemadiv = $smadiv->fields['MAILDIV'];
}

$qdiape = "SELECT /* + INDEX(E EMPL_UK_IDX) */  E.IDENTIFICACION CCDIAPE
                 ,E.EMAIL_INSTITUCIONAL MAILDIAPE
             FROM EMPLEADOS E
                 ,CARGOS C
            WHERE E.CARG_CARGO = C.CARGO
              AND E.CARG_FUERZA = C.FUERZA
              AND C.CARGO = 1903
              AND E.UNDE_CONSECUTIVO_LABORANDO = 50427
              AND E.ACTIVO = 'SI'";
$sdiape = $conn->Execute($qdiape);
//$row = oci_execute($sdiape);
//if($row = oci_fetch_array($sdiape, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$sdiape->EOF){
   $vdiape = $sdiape->fields['CCDIAPE'];
   $vmaildiape = $sdiape->fields['MAILDIAPE'];
}

//------------------------GLOBOS DE NOTIFICACION----------------------------//
if ($vdiape == $cedula) {
    $qsimbolo = "SELECT /* + ORDERED(S) */ COUNT(S.CEDULARE) SIMBOLO
                   FROM SOL_RETIROS_WEB S
                  WHERE S.APROBOJE = 1
                    AND S.APROBODIA IS NULL
                    AND S.VIGENTE = 1";
    $ssim = $conn->Execute($qsimbolo);
    //$row = oci_execute($ssim);
    //if($row = oci_fetch_array($ssim, OCI_ASSOC+OCI_RETURN_NULLS)){
    if(!$ssim->EOF){
       $vsimbo = $ssim->fields['SIMBOLO'];
    }
} else {
   $qsimbolo = "SELECT /* + ORDERED(S) */ COUNT(S.CEDULARE) SIMBOLO
                   FROM SOL_RETIROS_WEB S
                  WHERE S.CEDUJEFE = '".$cedula."'
                    AND S.APROBOJE IS NULL
                    AND S.VIGENTE = 1";
    $ssim = $conn->Execute($qsimbolo);
    //$row = oci_execute($ssim);
    //if($row = oci_fetch_array($ssim, OCI_ASSOC+OCI_RETURN_NULLS)){
    if(!$ssim->EOF){
       $vsimbo = $ssim->fields['SIMBOLO'];
    }
}
//--------------------------------------------------------------------------//

$qcreti = "SELECT /* + ORDERED(S) */  UNIQUE S.CEDULARE CERETI
             FROM SOL_RETIROS_WEB S
            WHERE S.CEDULARE = '".$cedula."'";
$retira = $conn->Execute($qcreti);
//$row = oci_execute($retira);
//if($row = oci_fetch_array($retira, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$retira->EOF){
   $vcreti = $retira->fields['CERETI'];
}else{
    $vcreti = 0;
}

$qcjefe = "SELECT /* + ORDERED(S) */  UNIQUE S.CEDUJEFE CEJEFE
             FROM SOL_RETIROS_WEB S
            WHERE S.CEDUJEFE = '".$cedula."'";
$jefe = $conn->Execute($qcjefe);
//$row = oci_execute($jefe);
//if($row = oci_fetch_array($jefe, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$jefe->EOF){
   $vcjefe = $jefe->fields['CEJEFE'];
}else{
    $vcjefe = 0;
}

$qcdia = "SELECT /* + ORDERED(S) */  UNIQUE S.CEDUDIAPE CEDIAPE
            FROM SOL_RETIROS_WEB S
           WHERE S.CEDUDIAPE = '".$cedula."'";
$diape = $conn->Execute($qcdia);
//$row = oci_execute($diape);
//if($row = oci_fetch_array($diape, OCI_ASSOC+OCI_RETURN_NULLS)){
if(!$diape->EOF){
   $vcdia = $diape->fields['CEDIAPE'];
}else{
    $vcdia = 0;
}

$qresuret = "SELECT /* + INDEX(E EMPL_UK_IDX) ORDERED(E,S) */  TO_CHAR(S.FECHASOL,'DD/MM/YYYY') FECHASOL
                   ,E.GRAD_ALFABETICO GRADO
                   ,E.NOMBRES||' '||E.APELLIDOS NOMBRES
                   ,DECODE(S.APROBOJE,1,'SI',0,'NO','') APROBOJE
                   ,TO_CHAR(S.FECHAAPJE,'DD/MM/YYYY') FECHAAPJE
                   ,DECODE(S.APROBODIA,1,'SI',0,'NO','') APROBODIA
                   ,TO_CHAR(S.FECHAAPDI,'DD/MM/YYYY') FECHAAPDI
              FROM SOL_RETIROS_WEB S
                  ,EMPLEADOS E 
             WHERE S.CEDULARE = E.IDENTIFICACION
               AND S.UNDE_FUERZA = E.UNDE_FUERZA
               AND S.CEDULARE ='".$vcreti."'
               ORDER BY S.IDSOLIRE";

$qresujef = "SELECT /* + INDEX(E EMPL_UK_IDX) ORDERED(E,S) */ TO_CHAR(S.FECHASOL,'DD/MM/YYYY') FECHASOL
                   ,E.GRAD_ALFABETICO GRADO
                   ,E.NOMBRES||' '||E.APELLIDOS NOMBRES
                   ,S.CEDULARE
               FROM SOL_RETIROS_WEB S
                   ,EMPLEADOS E 
              WHERE S.CEDULARE = E.IDENTIFICACION
                AND S.UNDE_FUERZA = E.UNDE_FUERZA
                AND S.CEDUJEFE = '".$vcjefe."'
                AND S.APROBOJE IS NULL
                AND S.VIGENTE = 1";

$qresudia = "SELECT /* + INDEX(E EMPL_UK_IDX) ORDERED(E,S) */ TO_CHAR(S.FECHASOL,'DD/MM/YYYY') FECHASOL
                   ,E.GRAD_ALFABETICO GRADO
                   ,E.NOMBRES||' '||E.APELLIDOS NOMBRES
                   ,DECODE(S.APROBOJE,1,'SI',0,'NO','') APROBOJE
                   ,TO_CHAR(S.FECHAAPJE,'DD/MM/YYYY') FECHAAPJE
                   ,S.CEDULARE
               FROM SOL_RETIROS_WEB S
                   ,EMPLEADOS E
              WHERE S.CEDULARE = E.IDENTIFICACION
                AND S.UNDE_FUERZA = E.UNDE_FUERZA
                AND S.CEDUDIAPE = '".$vcdia."'
                AND S.APROBOJE = 1
                AND S.VIGENTE = 1";

$todosol = "SELECT /* + INDEX(E EMPL_UK_IDX) ORDERED(E,S)*/  
                    TO_CHAR(S.FECHASOL,'DD/MM/YYYY') FECHASOL
                   ,E.GRAD_ALFABETICO GRADO
                   ,E.NOMBRES||' '||E.APELLIDOS NOMBRES
                   ,DECODE(S.APROBOJE,1,'SI',0,'NO','') APROBOJE
                   ,TO_CHAR(S.FECHAAPJE,'DD/MM/YYYY') FECHAAPJE
                   ,DECODE(S.APROBODIA,1,'SI',0,'NO','') APROBODIA
                   ,TO_CHAR(S.FECHAAPDI,'DD/MM/YYYY') FECHAAPD
                   ,S.IDSOLIRE
              FROM SOL_RETIROS_WEB S
                  ,EMPLEADOS E 
             WHERE S.CEDULARE = E.IDENTIFICACION
               AND S.UNDE_FUERZA = E.UNDE_FUERZA
               AND S.VIGENTE = 0
               AND E.UNDE_CONSECUTIVO_LABORANDO = $ulabora
                group by s.cedulare,S.FECHASOL,E.NOMBRES,E.APELLIDOS,S.APROBOJE,S.FECHAAPJE,S.APROBODIA,S.FECHAAPDI,E.GRAD_ALFABETICO,S.IDSOLIRE
                ORDER BY S.IDSOLIRE";

$todos = "SELECT /* + INDEX(E EMPL_UK_IDX) ORDERED(E,S)*/  
                    TO_CHAR(S.FECHASOL,'DD/MM/YYYY') FECHASOL
                   ,E.GRAD_ALFABETICO GRADO
                   ,E.NOMBRES||' '||E.APELLIDOS NOMBRES
                   ,DECODE(S.APROBOJE,1,'SI',0,'NO','') APROBOJE
                   ,TO_CHAR(S.FECHAAPJE,'DD/MM/YYYY') FECHAAPJE
                   ,DECODE(S.APROBODIA,1,'SI',0,'NO','') APROBODIA
                   ,TO_CHAR(S.FECHAAPDI,'DD/MM/YYYY') FECHAAPD
                   ,S.IDSOLIRE
              FROM SOL_RETIROS_WEB S
                  ,EMPLEADOS E 
             WHERE S.CEDULARE = E.IDENTIFICACION
               AND S.UNDE_FUERZA = E.UNDE_FUERZA
               AND S.VIGENTE = 0
               AND E.UNDE_CONSECUTIVO_LABORANDO = $ulabora
                group by s.cedulare,S.FECHASOL,E.NOMBRES,E.APELLIDOS,S.APROBOJE,S.FECHAAPJE,S.APROBODIA,S.FECHAAPDI,E.GRAD_ALFABETICO,S.IDSOLIRE
                ORDER BY S.IDSOLIRE";
               
?>