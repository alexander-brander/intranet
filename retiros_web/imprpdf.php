<?php
require_once('Conexiones/imprsol.php');
include ('fpdf/fpdf.php');

$simprpdf = $conn->Execute($qcontenido);
//$row = oci_execute($simprpdf);
//while ($row = oci_fetch_array($simprpdf, OCI_ASSOC+OCI_RETURN_NULLS)) {
while ($row = $simprpdf->FetchNextObject()){

$solpdf = new FPDF('P','mm','Letter');
$solpdf -> Addpage();
#Establecemos los márgenes izquierda, arriba y derecha:
$solpdf->SetMargins(30, 25 , 30);
#Establecemos el margen inferior:
$solpdf->SetAutoPageBreak(true,25); 
$solpdf -> Ln(20);
$solpdf -> SetFont('Arial','',11);
$solpdf -> Cell(0,10,utf8_decode($row->CIU_SOLI).', '.utf8_decode($row->FECHASOL));
$solpdf -> Ln(20);
$solpdf -> SetFont('Arial','',11);
$solpdf -> Cell(0,10,utf8_decode('Señor'));
$solpdf -> Ln(4);
$solpdf -> SetFont('Arial','B',11);
$solpdf -> Cell(0,10,utf8_decode($row->A));
$solpdf -> Ln(4);
$solpdf -> SetFont('Arial','',11);
$solpdf -> Cell(0,10,utf8_decode($row->CON));
$solpdf -> Ln(4);
$solpdf -> Cell(0,10,utf8_decode($row->EN));
$solpdf -> Ln(4);
$solpdf -> Cell(0,10,utf8_decode($row->LUGAR));
$solpdf -> Ln(20);
$solpdf -> Cell(0,10,utf8_decode('Asunto: Solicitud Retiro Voluntario Armada Nacional.'));
$solpdf -> Ln(20);
$solpdf -> MultiCell(0,4,utf8_decode($row->CONTENIDO),0);
$solpdf -> Ln(10);
$solpdf -> Cell(0,10,utf8_decode('Fecha de Retiro: ').utf8_decode($row->FECHARET));
$solpdf -> Ln(15);
$solpdf -> Cell(0,10,utf8_decode('Recibo Comunicación en:  '));
$solpdf -> Ln(7);
$solpdf -> MultiCell(0,4,utf8_decode('Dirección: ').utf8_decode($row->DIRECCION).', '.utf8_decode($row->CIUDAD).', '.utf8_decode($row->DPTO),0,'J');
$solpdf -> Cell(0,4,utf8_decode('Teléfono: ').utf8_decode($row->TELEFONO));
$solpdf -> Ln(4);
$solpdf -> Cell(0,4,'Email: '.$row->EMAIL);
$solpdf -> Ln(4);
$solpdf -> Cell(0,4,utf8_decode($row->COMUNICADO));
$solpdf -> Ln(15);
$solpdf -> Cell(0,10,utf8_decode('Respetuosamente;'));
$solpdf -> Ln(20);
$solpdf -> SetFont('Arial','B',11);
$solpdf -> Cell(0,10,$row->GRADO.' '.utf8_decode(mb_strtoupper($row->NOMBRES,'utf-8'))); 
$solpdf -> Ln(4);
$solpdf -> SetFont('Arial','',11);
$solpdf -> Cell(0,10,utf8_decode('Cedula de Ciudadanía ').$row->IDENTIFICACION);
$solpdf -> Output();
}
?>