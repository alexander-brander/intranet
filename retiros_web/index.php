<?php
require_once('Conexiones/consultar.php');
require_once('Conexiones/validacion.php');
error_reporting (-1);
$cedula = $_GET['usr_cc'] ;?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Retiros Web</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body style = "background: url('images/fondo4.jpg') no-repeat center center fixed;">
  	<div class="container"style="filter:alpha(opacity=100); opacity:0.8;">
		<div class="col-md-1"></div>
		<div class="col-md-10">
	    		<div class="panel-group" style="margin-bottom: 0px;">
	    			<div class="panel panel-primary" style="padding-bottom: 0px; padding-top: 0px;">
	    				<div class="panel-heading" >
	    					<h3><p class="text-center"><b>Retiros en Linea</b></p></h3>
	    				</div>
	    				<div class="panel-body text-right" >Dirección de Personal<br>Division de Hojas de Vida<br>Sección SIATH
	    				</div>
	    			</div>
	    		</div>
		<nav class="navbar navbar-inverse" style="margin-bottom: 0px;">
		  <div class="container">
		    <div class="navbar-header">
		      <a class="navbar-brand"><?php echo $vgrado."<span class='text-capitalize'> ".$vnombre."</span>\n"?></a>
		        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
		    </div>		    
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="index.php?usr_cc=<?php echo $cedula;?>">Información Básica <span class="glyphicon glyphicon-list-alt"></span></a></li>
		        <?php if ($vcontso == 0) {echo "<li><a href='solicitud.php?usr_cc=".$cedula."'>Generar Solicitud <span class='glyphicon glyphicon-pencil'></span></a></li>\n";}?>
		        <?php if ($vcreti != 0 || $vcjefe != 0 || $vcdia != 0) {echo "<li><a href='validar.php?usr_cc=".$cedula."'>Validar <span class='glyphicon glyphicon-ok'></span>\n";}?>
		        <?php if ($vsimbo > 0 ) {echo "<span class='badge'>".$vsimbo."</span>\n";}?></a>
		        <li><a href="https://marinanet.armada.mil.co/">Salir</a></li>
		      </ul>
		    </div>
		  </div>
		</nav>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="container">
		<div class="col-md-1"></div>
		<div class="panel-group  col-md-10">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<p class="text-capitalize text-center"><b>Actualmente tiene registrado en el sistema de<br>
					administración para el talento humano (SIATH)</b></p>
			</div>
				<div class="panel panel-info">
					<div class="panel-heading">
						<p class="text-capitalize text-left"><b>Tiempos:</b></p>
					</div>
					<div class="panel-body">
					<div class="table-responsive">
						<?php
						$stid = $conn->Execute($query);
						//oci_execute($stid);	
						echo "<table class='table table-condensed'>\n";
						echo "<tr>\n";
							echo "<th class='text-center text-capitalize'>novedades</th>";
							echo "<th class='text-center text-capitalize'>fecha</th>";
							echo "<th class='text-center text-capitalize'>fecha</th>";
							echo "<th class='text-center text-capitalize'>total</th>";
							echo "</tr>\n";
							echo "<tr>\n";
							echo "<th class='text-center text-capitalize'>tiempos</th>";
							echo "<th class='text-center text-capitalize'>desde</th>";
							echo "<th class='text-center text-capitalize'>hasta</th>";
							echo "<th class='text-center'>AA-MM-DD</th>";
							echo "</tr>\n";
						    echo "<tr>\n";
					    //while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
					     /*foreach ($stid as $item) {
					    }*/
					    //oci_execute($stid);
					    //while ($fila = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
					    while ($row = $stid->FetchNextObject()){
						   echo "<td class='text-center text-capitalize '>".$row->DESCRIPCION."</td>\n";
						   echo "<td class='text-center text-capitalize '>".$row->F_INICIO."</td>\n";
						   echo "<td class='text-center text-capitalize '>".$row->F_FIN."</td>\n";
						   echo "<td class='text-center text-capitalize '>".$row->TOTAL."</td>\n";
						   echo "</tr>\n";						   
						  }
					    
						$fila = $conn->Execute($queryb);
						echo "<tr>\n";
						while ($row = $fila->FetchNextObject()) {
					    	 echo "<td> </td><td> </td><td> </td> <td class='text-center'>".$row->TIEMPO."</td>\n";
					    }
					    echo "</tr>\n";
						echo "</table>\n"; ?>
					</div>
					<div class="panel panel-<?php if ($vanios >= '190900') {
												    echo 'success';
												  } else {
												    echo 'danger';}
										    ?>">
				      <div class="panel-heading" data-toggle="tooltip"  title="Información Suministrada por la División de Hojas de Vida.!"><?php if ($vanios >= '190900') {
												    echo 'C';
												  } else {
												    echo 'No c';}
										    ?>umple el tiempo para tener derecho a la asignación de retiro.</div>
				    </div>
				    </div>
				</div>
				<div class="panel panel-info">
					<div class="panel-heading">
						<p class="text-capitalize text-left"><b>Estado Civil: </b><?php echo $vestado?></p>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
						<?php
						$stid = $conn->Execute($qbenefi);
						//oci_execute($stid);
						echo "<table class='table table-condensed'>\n";
						echo "<tr>\n";
						echo "<th class='text-center text-capitalize'>parentesco</th>";
						echo "<th class='text-center text-capitalize'>nombres</th>";
						echo "<th class='text-center text-capitalize'>documento</th>";
						echo "<th class='text-center text-capitalize'>f. nacimiento</th>";
						echo "<th class='text-center text-capitalize'>edad</th>";
						echo "</tr>\n";
					    echo "<tr>\n";
					    /*while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
					    foreach ($row as $item) {
					        echo "    <td class='text-center text-capitalize '>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "") . "</td>\n";
					    }*/
					    while ($row = $stid->FetchNextObject()) {
					    	 echo "<td class='text-center text-capitalize '>" .$row->PARENTESCO. "</td>\n";
					    	 echo "<td class='text-center text-capitalize '>" .$row->NOMBRES. "</td>\n";
					    	 echo "<td class='text-center text-capitalize '>" .$row->DOCUMENTO. "</td>\n";
					    	 echo "<td class='text-center text-capitalize '>" .$row->F_NAC. "</td>\n";
					    	 echo "<td class='text-center text-capitalize '>" .$row->EDAD. "</td>\n";
					    	 echo "</tr>\n";
					    }					    
					    echo "</tr>\n";
						echo "</table>\n"; ?>
					</div>
					<?php if ($vhijo > 0){
						echo "<div class='panel panel-warning'>\n";
				      	echo "<div class='panel-heading' data-toggle='tooltip'  title='Información Suministrada por la División de Nominas.!'>Requiere Acreditar que su(s) hijo(s)(as) mayor(res) de 21 años esta(n) estudiando.</div>\n";
				    	echo "</div>\n";
					} ?>
				    <?php if ($vgrado == 'CN' || $vgrado == 'CR'){
				    	echo "<div class='panel panel-warning'>\n";
				        echo "<div class='panel-heading' data-toggle='tooltip'  title='Información Suministrada por la División de Hojas de Vida.!'><b>Prima de Estado Mayor:</b><br>\n";
				      	echo "Requiere acreditar que finalizó satisfactoriamente el Curso de Estado Mayor.</div>\n";
				        echo "</div>\n";}?>
				    <?php if($vvuelo > 0){
				    	echo "<div class='panel panel-warning'>\n";
				    	echo "<div class='panel-heading' data-toggle='tooltip'  title='Información Suministrada por la División de Hojas de Vida.!'><b>Prima de Vuelo:</b><br>\n";
				    	echo "Requiere acreditar las horas de vuelo.</div>\n";
				        echo "</div>\n";}?>
					</div>
				</div>
				<div class="panel panel-info">
					<div class="panel-heading">
						<p class="text-capitalize text-left"><b>Embargos:</b></p>
					</div>
					<div class="panel-body">
							<?php
							if ($venti != ''){
							echo "<div class='table-responsive'>";

							echo "<table class='table table-condensed'>\n";
							echo "<tr>\n";
							echo "<th class='text-center text-capitalize'>entidad</th>";
							echo "<th class='text-center text-capitalize'>desde</th>";
							echo "<th class='text-center text-capitalize'>hasta</th>";
							echo "<th class='text-center text-capitalize'>monto</th>";
							echo "</tr>\n";
						    echo "<tr>\n";
							/*$row = oci_execute($stem);
						    while ($row = oci_fetch_array($stem, OCI_ASSOC+OCI_RETURN_NULLS)) {
						    foreach ($row as $item) {
						        echo "    <td class='text-center text-capitalize '>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "") . "</td>\n";
						    }
						    }*/
						    $stem = $conn->Execute($qembargo);
						    while ($row = $stem->FetchNextObject()) {
						    	 echo "<td class='text-center text-capitalize '>" .$row->ENTIDAD. "</td>\n";
						    	 echo "<td class='text-center text-capitalize '>" .$row->DESDE. "</td>\n";
						    	 echo "<td class='text-center text-capitalize '>" .$row->HASTA. "</td>\n";
						    	 echo "<td class='text-center text-capitalize '>" .$row->MONTO. "</td>\n";
						    	 echo "</tr>\n";
						    }						    
							echo "</table>\n";
							echo "</div>";
							}else {
								echo "<div class='panel panel-success'>\n
									      <div class='panel-heading'><b>No posee</b></div>\n
									  </div>\n";}
							?>
						<div class="panel panel-warning">
					      <div class="panel-heading" data-toggle="tooltip"  title="Información Suministrada por Control Vacaciones(DIAPE).!"><b>Vacaciones:</b> Se programó para el <?php print $vvac?></div>
					    </div>
					    <div class="panel panel-default">
					      <div class="panel-heading" data-toggle="tooltip"><b>NOTA:</b> Los datos aquí presentados están sujetos a verificación.</div>
					    </div>
					</div>
				</div>
			<?php if ($vcontso == 0) {
			echo "<nav>\n";
			  echo "<ul class='pager'>\n";
			    echo "<li><a href='solicitud.php?usr_cc=".$cedula."'>Continuar</a></li>\n";
			  echo "</ul>\n";
			echo "</nav>\n";
			}?>
		</div>
	    </div>
	</div>
	<div class="col-md-1"></div>
  <script src="bootstrap/js/jquery-1.11.2.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();
		});
  </script>
</body>
</html>