<?php 
require_once('Conexiones/consultar.php');
error_reporting (-1);
$cedula = $_GET['usr_cc'] ;
session_start();?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Retiros Web</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/extra.css" rel="stylesheet">   
  <link rel="stylesheet" href="/resources/demos/style.css">
  </head>
<body style = "background:url('images/fondo4.jpg') no-repeat center center fixed;">
  	<div class="container"style="filter:alpha(opacity=100); opacity:0.8;">
		<div class="col-md-1"></div>
		<div class="col-md-10">
    		<div class="panel-group" style="margin-bottom: 0px;">
    			<div class="panel panel-primary" style="padding-bottom: 0px; padding-top: 0px;">
    				<div class="panel-heading" >
    					<h3><p class="text-center"><b>Retiros en Linea</b></p></h3>
    				</div>
    				<div class="panel-body text-right" >Dirección de Personal<br>Division de Hojas de Vida<br>Sección SIATH
    				</div>
    			</div>
    		</div>
			<nav class="navbar navbar-inverse" style="margin-bottom: 0px;">
				<div class="container">
				    <div class="navbar-header">
				      	<a class="navbar-brand"><?php print $vgrado."<span class='text-capitalize'> ".$vnombre."</span>\n"?></a>
				        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					    </button>
				    </div>
				    <div class="collapse navbar-collapse" id="myNavbar">
				    	<ul class="nav navbar-nav">
				    		<li><a href="index.php?usr_cc=<?php echo $cedula;?>">Información Básica <span class="glyphicon glyphicon-list-alt"></span></a></li>
				        	<li><a href="javascript:history.back(1)">Generar Solicitud <span class="glyphicon glyphicon-pencil"></span></a></li>
				      	</ul>
				    </div>
			  	</div>
			</nav>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="container">
		<div class="col-md-1"></div>
		<div class="panel-group col-md-10">
			<div class="panel panel-info">
				<div class="panel-heading">
					<p class="text-capitalize text-center"><b>Enviar Solicitud</b></p>
				</div>
				<div id="sol" class="panel panel-info">
					<div class="container">
						<br>
						<div class="col-md-1"></div>
						<div class="col-md-7">
							<?php
							$_SESSION['variable1'] = $_REQUEST['variable1'];
							if ($vnumeri <= 4) {
								$presi = "JUAN MANUEL SANTOS CALDERON";
								} elseif ($vnumeri > 4 && $vnumeri <= 9 ) {
									$presi = "LUIS CARLOS VILLEGAS";
								}else {
									$presi = "LEONARDO SANTAMARIA GAITAN";
								}
							if (isset($_POST['variable10'])) {
								$depar10="Si";
							} else {
								$depar10="No";
							}
							$_SESSION['variable10'] = $depar10;
							$_SESSION['variable2'] = $_REQUEST['variable2'];
							$_SESSION['fecha'] = $_REQUEST['fecha'];
							$_SESSION['variable6'] = $_REQUEST['variable6'];
							$_SESSION['variable8'] = $_REQUEST['variable8'];
							$_SESSION['departa'] = $_REQUEST['departa'];
							$_SESSION['variable9'] = $_REQUEST['variable9'];
							$_SESSION['variable11'] = $_REQUEST['variable11'];
							$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
							$mes = date('d')." de ".$meses[date('n')-1]. " de ".date('Y');
							$_SESSION['mes'] = $mes;
							$_SESSION['variable3'] = $presi;
							$depar1 = $_POST['variable1'];
							$depar2 = $_POST['variable2'];
							$depar3 = $_POST['fecha'];
							$depar6 = $_POST['variable6'];
							$depar7 = $_POST['departa'];
							$depar8 = $_POST['variable8'];
							$depar11 = $_POST['variable11'];
							$depar9 = $_POST['variable9'];

							echo "<p id='cargaexterna'>\n";
							echo $depar1.", ";
							echo $mes;
							echo "<br><br><br><br>
							<p class='text-left'>
					    		Señor:<br>\n";
					    		if ($vnumeri <= 4) {
										echo "<b>JUAN MANUEL SANTOS CALDERON</b><br>\n";
					    				echo "Presidente de la Republica<br>\n";
					    				echo "Casa de Nariño<br>\n";
									} elseif ($vnumeri > 4 && $vnumeri <= 9 ) {
										echo "<b>LUIS CARLOS VILLEGAS</b><br>\n";
					    				echo "Ministro de Defensa<br>\n";
					    				echo "Ministerio de Defensa Nacional<br>\n";
									}else {
										echo "<b>LEONARDO SANTAMARIA GAITAN</b><br>\n";
					    				echo "Almirante<br>\n";
					    				echo "Armada Nacional<br>\n";
									}
					    	echo "Bogotá (Bogotá D.C.)<br><br><br>
					    		Asunto: Solicitud Retiro Voluntario Armada Nacional.
						    </p><br>\n";
							echo $depar2;
							echo "<br><br>\n";
							echo "Fecha de Retiro: "; if ($_SESSION['fecha'] == '') { echo "Acuerdo Expedición Acto Administrativo\n"; } else { echo $depar3;}
							echo "<br><br><br>\n";
							echo "Recibo Comunicación en: <br>Dirección: ".$depar6;
							echo ", ";
							echo $depar8;
							echo ", ";
							echo $depar7;
							echo "</br> Telefono: ";
							echo $depar9;
							echo "<br> Email: ";
							echo $depar11;
							echo "<br>";
							echo $depar10;
							echo " Acepto Ser Cumincado por Correo Electrónico</p>\n";
							echo " <br><br> Respetuosamente;<br><br><br><br><br>
					    		   <b>$vgrado <span class='text-uppercase'>$vnombre.</span></b>
					    		   <br>Cedula de Ciudadanía $cedula<br>\n";
					    	echo "<p> <p><br>\n";
					    	echo "<p> <p><br>\n";
							?>
						</div>
					</div>
				</div>
				<nav>
					<ul class="pager">
						<li><a href="javascript:history.back(1)">Volver</a></li>
				  		<li><a href="solicitud.php?usr_cc=<?php echo $cedula;?>">Eliminar</a></li>
				  		<li><a data-toggle="modal" data-target="#myModal">Imprimir</a></li>			  		
				  	</ul>
				</nav>
			</div>
	    </div>
	     <!-- Modal -->
		  <div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog modal-lg">
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Jefe Inmediato</h4>
		        </div>
		        <div class="table-responsive">
						<?php
						echo "<table class='table table-hover'>\n";
						echo "<tr>\n";
							echo "<th class='text-center'>GRADO</th>";
							echo "<th class='text-center'>CEDULA</th>";
							echo "<th class='text-center'>NOMBRES</th>";
							echo "<th class='text-center'>CARGO</th>";
							echo "<th class='text-center'>UNIDAD</th>";
							echo "<th class='text-center'>SELECCIONAR</th>";
							echo "</tr>\n";
							if ($vceduje == $cedula) {
								$spapa = $conn->Execute($qpapa);
								//$row = oci_execute($spapa);	
								//while ($row = oci_fetch_array($spapa, OCI_ASSOC+OCI_RETURN_NULLS)) {
								while ($row = $spapa->FetchNextObject()){
								        echo "<td class='text-center'>".$row->GRAPA."</td>";
								        echo "<td class='text-center'>".$row->CEDUPA."</td>";
								        echo "<td class='text-center'>".$row->NOMPA."</td>";
								        echo "<td class='text-center'>".$row->CARPA."</td>";
								        echo "<td class='text-center'>".$row->UNIPA."</td>";
								        echo "<td class='text-center'><input type='radio' name='optradio' value='".$row->CEDUPA."'></td>";
								        echo "<input type='hidden' id='ema' value='".$row->MAILPA."'>";
								    echo "</tr>\n";
								    }								    
							} else {
								$sjefe = $conn->Execute($qjefe);
								//$row = oci_execute($sjefe);
								//while ($row = oci_fetch_array($sjefe, OCI_ASSOC+OCI_RETURN_NULLS)) {
								while ($row = $sjefe->FetchNextObject()){
								        echo "<td class='text-center'>".$row->GRAJE."</td>";
								        echo "<td class='text-center'>".$row->CEDUJE."</td>";
								        echo "<td class='text-center'>".$row->NOMJE."</td>";
								        echo "<td class='text-center'>".$row->CARJE."</td>";
								        echo "<td class='text-center'>".$row->UNIJE."</td>";
								        echo "<td class='text-center'><input type='radio' name='optradio' value='".$row->CEDUJE."'></td>";
								        echo "<input type='hidden' id='ema' value='".$row->MAILJE."'>";
								    echo "</tr>\n";
								    }
							}
					    echo "</tr>\n";
						echo "</table>\n"; ?>
					</div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal" id="impr">Imprimir</button>
		           <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
	</div>
	<script src="bootstrap/js/jquery-1.11.2.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript">
		$("#impr").click(function(){
			opciones = document.getElementsByName("optradio");
			ema = document.getElementById("ema").value;
				var seleccionado = false;
				for(var i=0; i<opciones.length; i++) {    
				  if(opciones[i].checked) {
				    seleccionado = true;
				    if (seleccionado == true) {
				    	var ceje = opciones[i].value;
						document.cookie ='p_jefe='+ceje; //+'; expires=Thu, 2 Aug 2050 20:47:11 UTC; path=/';
						document.cookie ='p_ema='+ema;
						ced = <?php echo $cedula?>;
				    	window.open("solpfd.php?usr_cc="+ced,"Solicitud PDF");
				    	window.open("mail/sendmail.php?usr_cc="+ced,"Envio correo");
				    	setTimeout ("window.location='index.php?usr_cc="+ced+"';", 5000);
				    }
				    break;
				  }
				}
				if(!seleccionado) {
				  return false;
				}
		  	});
	</script>
</body>
</html>
