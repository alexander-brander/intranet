<?php 
require_once('Conexiones/consultar.php');
error_reporting (-1);
$cedula = $_GET['usr_cc'] ;?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Retiros Web</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"/>
  <script src="bootstrap/js/jquery-1.11.2.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="jquery.js"></script>
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
  <script src="jquery.ui.datepicker-es.js"></script>
  </head>
<body style = "background:url('images/fondo4.jpg') no-repeat center center fixed;">
  	<div class="container"style="filter:alpha(opacity=100); opacity:0.8;">
		<div class="col-md-1"></div>
		<div class="col-md-10">
	    		<div class="panel-group" style="margin-bottom: 0px;">
	    			<div class="panel panel-primary" style="padding-bottom: 0px; padding-top: 0px;">
	    				<div class="panel-heading" >
	    					<h3><p class="text-center"><b>Retiros en Linea</b></p></h3>
	    				</div>
	    				<div class="panel-body text-right" >Dirección de Personal<br>Division de Hojas de Vida<br>Sección SIATH
	    				</div>
	    			</div>
	    		</div>
		<nav class="navbar navbar-inverse" style="margin-bottom: 0px;">
		  <div class="container">
		    <div class="navbar-header">
		      <a class="navbar-brand"><?php echo $vgrado."<span class='text-capitalize'> ".$vnombre."</span>\n"?></a>
		        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>                     
			    </button>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav">
		        <li><a href="index.php?usr_cc=<?php echo $cedula;?>">Información Básica <span class="glyphicon glyphicon-list-alt"></span></a></li>
		        <li class="active"><a href="solicitud.php?usr_cc=<?php echo $cedula;?>">Generar Solicitud <span class="glyphicon glyphicon-pencil"></span></a></li>
		      </ul>
		    </div>
		  </div>
		</nav>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="container">
		<div class="col-md-1"></div>
		<div class="panel-group col-md-10">
		<div class="panel panel-info">
			<div class="panel-heading">
				<p class="text-capitalize text-center"><b>Solicitud de Retiro Armada Nacional</b></p>
			</div>
			<div id="sol" class="panel panel-info">
				<div class="container">
					<br>
					<div class="col-md-1"></div>
					<div class="col-md-7">
						<form class="col-xs-11 col-sm-11 col-md-12" name="infor" action="radicar.php?usr_cc=<?php echo $cedula;?>" method="post" >
							<div class="form-group">
								<label class="control-label" for="ciudad">Ciudad:</label>
									<select class="form-control col-sm-5" name="variable1" id="desde">
										<option>Seleccione..</option>
										<?php
										$sciu = $conn->Execute($qciuda);
										//while ($row = oci_fetch_array($sciu, OCI_ASSOC+OCI_RETURN_NULLS)) {
										while ($row = $sciu->FetchNextObject()){
												echo "<option>\n";
												echo $row->CIUDAD;
												echo "</option>\n";
									    }?>
									</select>
							</div>
							<br><br><br>
							<p class="text-left ">
								Señor:<br>
								<?php
									if ($vnumeri <= 4 && $vnumeri >= 1) {
										echo "<b>JUAN MANUEL SANTOS CALDERON</b><br>\n";
					    				echo "Presidente de la República<br>\n";
					    				echo "Casa de Nariño<br>\n";
									} elseif ($vnumeri > 4 && $vnumeri <= 9 ) {
										echo "<b>LUIS CARLOS VILLEGAS</b><br>\n";
					    				echo "Ministro de Defensa<br>\n";
					    				echo "Ministerio de Defensa Nacional<br>\n";
									}else {
										echo "<b>LEONARDO SANTAMARIA GAITAN</b><br>\n";
					    				echo "Almirante<br>\n";
					    				echo "Armada Nacional<br>\n";
									}
								?>
					    		Bogotá (Bogotá D.C.)<br><br>
					    		Asunto: Solicitud Retiro Voluntario Armada Nacional.
						    </p><br>
							<div class="form-group">
								<textarea name="variable2" class="form-control" rows="10" id="solicitud" placeholder="Escriba su solicitud.." maxlength="1000"></textarea>
							</div>
							<label class="control-label">Fecha de Retiro: </label>&nbsp;&nbsp;
								<input type="text" name="fecha" id="datepicker">&nbsp;<input type="hidden" id="alternate" size="30">
							<br>
							<label class="control-label">Comunicación</label>
							<div class="form-group">
							    <label class="control-label">Dirección</label>
							    <input name="variable6" type="text" class="form-control" id="dir" placeholder="Ingrese la Dirección de Residencia.">
							</div>
							<div class="form-group">
							    <label class="control-label">Departamento</label>
							    <select id ="depto" class="form-control col-sm-5" name="variable7">
							    	<option>Seleccione..</option>
								  	<?php
								  	$sdpto = $conn->Execute($qdpto);
									//while ($row = oci_fetch_array($sdpto, OCI_ASSOC+OCI_RETURN_NULLS)) {
								  	while ($row = $sdpto->FetchNextObject()){
											echo "<option value=".$row->CODIGO.">\n";
											echo utf8_encode($row->DPTO);
											echo "</option>\n";
								    }?>
								</select>
								<input name="departa" id="departa" type="text" class="hidden">
							</div>
							<div class="form-group">
							    <label class="control-label">Ciudad</label>							    
							    <select id ="ciud" class="form-control col-sm-5" name="variable8">
								</select>
							</div>
							<div class="form-group">
							    <label class="control-label">Telefono</label>
							    <input name="variable9" type="text" class="form-control" id="tel" placeholder="Ingrese el Número de Celular">
							    <label class="control-label">Email</label>
							    <input type="email" class="form-control" id="email" placeholder="Ingrese el Correo Pesonal" name="variable11">
							</div><br>
							<div class="checkbox">
								<label><input type="checkbox" name="variable10"> Deseo ser Comunicado por Correo Electrónico</label>
					        </div>
					        <br><br>
					        <p class="text-left ">
					    		Respetuosamente;<br><br><br>
					    		<b><?php print $vgrado."<span class='text-uppercase'> ".$vnombre."</span>\n"?></b><br>
					    		Cedula de Ciudadanía <?php print $cedula?><br>
						    </p><br>
							<center><button type="submit" id="continuar" class="btn btn-primary" onclick="return validar()">Continuar</button></center>
						</form>
					</div>
				</div>
			</div>
		</div>	
	    </div>
	</div>	  
	  <script type="text/javascript">
		$(document).ready(function(){
			$("#depto").click(function(){
				var valor = $("#depto option:selected").html();
				$("#departa").val(valor);
				var id = $("#depto").val();
				$.get("ciudades.php",{p_depto:id})
				.done(function(data){
					$("#ciud").html(data);
				});
		  	});
		});
		function validar(){
			indice = document.getElementById("desde").selectedIndex;
			if( indice == null || indice == 0 ) {
				alert("No ha seleccionado el lugar desde donde se remite la solicitud.")
				return false;
			}

			valor = document.getElementById("solicitud").value;
			if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
				alert("No se ha digitado la solicitud.")
			  	return false;
			}

			dire = document.getElementById("dir").value;
			if( dire == null || dire.length == 0 || /^\s+$/.test(dire) ) {
				alert("No se ha registrado la dirección.")
			  	return false;
			}

			departo = document.getElementById("depto").selectedIndex;
			if( departo == null || departo == 0 ) {
				alert("No ha seleccionado el Departamento")
				return false;
			}

			ciu = document.getElementById("ciud").selectedIndex;
			if( ciu == null || ciu == 0 ) {
				alert("No ha seleccionado la Ciudad.")
				return false;
			}

			cel = document.getElementById("tel").value;
			if( !(/^\d{10}$/.test(cel)) ) {
				alert("No ha registrado el numero Celular.")
			  	return false;
			}

			mail = document.getElementById("email").value;
			if( !(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(mail)) ) {
				alert("Email no registrado o incorrecto.")
			  	return false;
			}
		}
	  </script>
	  <script>
	    $.datepicker.regional['es'] = {
			 closeText: 'Cerrar',
			 prevText: '<Ant',
			 nextText: 'Sig>',
			 currentText: 'Hoy',
			 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			 weekHeader: 'Sm',
			 dateFormat: 'dd/mm/yy',
			 firstDay: 1,
			 isRTL: false,
			 showMonthAfterYear: false,
			 yearSuffix: ''
		};
		$(function () {
		$.datepicker.setDefaults($.datepicker.regional['es']);
		$("#datepicker").datepicker({
		firstDay: 1,
		altField: "#alternate",
	    altFormat: "DD, d MM, yy"
		});
		});
	  </script>
</body>
</html>