<?php 
require('Conexiones/validacion.php');
setcookie('p_ced',''); 
setcookie('p_apr',''); 
setcookie('p_cedu',''); 
setcookie('p_apro','');
setcookie('p_ema','');
error_reporting (0);
$cedula = $_GET['usr_cc'];?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Retiros Web</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  </head>
<body style = "background: url('images/fondo4.jpg') no-repeat center center fixed;">
  	<div class="container"style="filter:alpha(opacity=100); opacity:0.8;">
		<div class="col-md-1"></div>
		<div class="col-md-10">
	    		<div class="panel-group" style="margin-bottom: 0px;">
	    			<div class="panel panel-primary" style="padding-bottom: 0px; padding-top: 0px;">
	    				<div class="panel-heading" >
	    					<h3><p class="text-center"><b>Retiros en Linea</b></p></h3>
	    				</div>
	    				<div class="panel-body text-right" >Dirección de Personal<br>Division de Hojas de Vida<br>Sección SIATH
	    				</div>
	    			</div>
	    		</div>
		<nav class="navbar navbar-inverse" style="margin-bottom: 0px;">
		  <div class="container">
		    <div class="navbar-header">
		      <a class="navbar-brand"><?php print $vgrado."<span class='text-capitalize'> ".$vnombre."</span>\n"?></a>
		        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav">
		        <li><a href="index.php?usr_cc=<?php echo $cedula;?>">Información Básica <span class="glyphicon glyphicon-list-alt"></span></a></li>
		        <?php if ($vcontso == 0) {echo "<li><a href='solicitud.php?usr_cc=".$cedula."'>Generar Solicitud <span class='glyphicon glyphicon-pencil'></span></a></li>\n";}?>
		        <?php if ($vcreti != 0 || $vcjefe != 0 || $vcdia != 0) {echo "<li class='active'><a href='validar.php?usr_cc=".$cedula."'>Validar <span class='glyphicon glyphicon-ok'></span>\n";}?>
		        <?php if ($vsimbo > 0 ) {echo "<span class='badge'>".$vsimbo."</span>\n";}?></a>
		      </ul>
		    </div>
		  </div>
		</nav>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="container">
		<div class="col-md-1"></div>
		<div class="panel-group  col-md-10">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<p class="text-capitalize text-center"><b>Solicitudes Realizadas y por Validar</b></p>
				</div>				
				<div class="panel-group" id="accordion">
					<?php if ($vcreti == $cedula) {
				    echo "<div class='panel panel-info'>\n";
				      echo "<div class='panel-heading'>\n";
				        echo "<h4 class='panel-title'>\n";
				          echo "<a data-toggle='collapse' data-parent='#accordion' href='#collapse1'>Estado Soliditud</a>\n";
				        echo "</h4>\n";
				      echo "</div>\n";
				      echo "<div id='collapse1' class='panel-collapse collapse in'>\n";
				        echo "<div class='panel-body'>\n";
				        	echo "<div class='table-responsive'>\n";
								$stid = $conn->Execute($qresuret);
								//oci_execute($stid);	
								echo "<table class='table table-bordered'>\n";
									echo "<tr>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Soliditud</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Grado</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Nombres</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Aprobó Jefe</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Apro/Rech</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Aprobó DIAPE</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Apr/Rec</th>\n";
									echo "</tr>\n";
									echo "<tr>\n";
									echo "</tr>\n";
								    echo "<tr>\n";

								    /*while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
								    foreach ($row as $item) {
								        echo "    <td nowrap class='text-center text-capitalize'>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "") . "</td>\n";
								    }
								    echo "</tr>\n";

								    }*/
								    while ($row = $stid->FetchNextObject()){
								    	echo "<td nowrap class='text-center text-capitalize'>" .$row->FECHASOL. "</td>\n";
								    	echo "<td nowrap class='text-center text-capitalize'>" .$row->GRADO. "</td>\n";
								    	echo "<td nowrap class='text-center text-capitalize'>" .$row->NOMBRES. "</td>\n";
								    	echo "<td nowrap class='text-center text-capitalize'>" .$row->APROBOJE. "</td>\n";
								    	echo "<td nowrap class='text-center text-capitalize'>" .$row->FECHAAPJE. "</td>\n";
								    	echo "<td nowrap class='text-center text-capitalize'>" .$row->APROBODIA. "</td>\n";
								    	echo "<td nowrap class='text-center text-capitalize'>" .$row->FECHAAPDI. "</td>\n";
								    	echo "</tr>\n";
								    }
								    echo "</table>\n";
							echo "</div>\n";
						echo "</div>\n";
				      echo "</div>\n";
				    echo "</div>\n";
				    }?>
				    <?php if ($vcjefe == $cedula) {
				    	if ($vsimbo > 0) {
				    		echo "<div class='panel panel-info'>\n";
						      echo "<div class='panel-heading'>\n";
						        echo "<h4 class='panel-title'>\n";
						          echo "<a data-toggle='collapse' data-parent='#accordion' href='#collapse2'>Confirmar Soliditudes</b></a>\n";
						       echo "</h4>\n";
						      echo "</div>\n";
						      echo "<div id='collapse2' class='panel-collapse collapse in'>\n";
						        echo "<div class='panel-body'>\n";
						        	echo "<div class='table-responsive'>\n";
										$stid = $conn->Execute($qresujef);
										//oci_execute($stid);	
										echo "<table class='table table-bordered'>\n";
											echo "<tr>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Soliditud</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Grado</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Nombres</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Ver Soliditud</th>\n";
											echo "<th class='text-center text-capitalize active' colspan='2'>Validar Soliditud</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Confirmar</th>\n";
											echo "</tr>\n";
											echo "<tr>\n";
											echo "<th class='text-center text-capitalize active'>Aprueba</th>\n";
											echo "<th class='text-center text-capitalize active'>Rechaza</th>\n";
											echo "</tr>\n";
										    echo "<tr>\n";

										   //while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
										   	while ($row = $stid->FetchNextObject()){
										        echo "<td class='text-center'>",$row->FECHASOL,"</td>";
										        echo "<td class='text-center'>",$row->GRADO,"</td>";
										        echo "<td class='text-center' nowrap>",$row->NOMBRES,"</td>";
										        $pfce = $row->CEDULARE;
										        echo "<input type='hidden' id='".$pfce."' value='".$row->CEDULARE."'>";
										        echo "<td class='text-center'><button type='button' class='btn btn-info btn-sm' id='impr' onclick='impr(".$pfce.")'>
										        	  <span class='glyphicon glyphicon-file'></span></button></td>";
										        echo "<td class='text-center'><input type='radio' name='".$pfce."' value='1'>";
										        echo "<td class='text-center'><input type='radio' name='".$pfce."' value='0'></td>";
										        echo "<td class='text-center'><button type='button' class='btn btn-success btn-sm' id='ok' onclick='capturar(".$pfce.")'>
													  <span class='glyphicon glyphicon-ok'></span></button></td>";
										    echo "</tr>\n";
										    }
										echo "</table>\n";
									echo "</div>\n";
						        echo "</div>\n";
						      echo "</div>\n";
						    echo "</div>\n";
						    echo "<div class='panel panel-info'>\n";
						      echo "<div class='panel-heading'>\n";
						        echo "<h4 class='panel-title'>\n";
						          echo "<a data-toggle='collapse' data-parent='#accordion' href='#collapse3'>Historial Soliditudes</b></a>\n";
						       echo "</h4>\n";
						      echo "</div>\n";
						      echo "<div id='collapse3' class='panel-collapse collapse'>\n";
						        echo "<div class='panel-body'>\n";
						        	echo "<div class='table-responsive'>\n";
										$stid = $conn->Execute($todosol);
										//oci_execute($stid);	
										echo "<table class='table table-bordered'>\n";
									echo "<tr>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Soliditud</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Grado</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Nombres</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Aprobó Jefe</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Apro/Rech</th>\n";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Aprobó DIAPE</th\n>";
									echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Apr/Rec</th>\n";
									echo "</tr>\n";
									echo "<tr>\n";
									echo "</tr>\n";
								    echo "<tr>\n";

								    //while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
								    while ($row = $stid->FetchNextObject()){
								        echo "<td class='text-center'>",$row->FECHASOL,"</td>";
										echo "<td class='text-center'>",$row->GRADO,"</td>";
										echo "<td class='text-center' nowrap>",$row->NOMBRES,"</td>";
										echo "<td class='text-center'>",$row->APROBOJE,"</td>";
										echo "<td class='text-center'>",$row->FECHAAPJE,"</td>";
										echo "<td class='text-center'>",$row->APROBODIA,"</td>";
										echo "<td class='text-center'>",$row->FECHAAPD,"</td>";
								    echo "</tr>\n";
								    }
								echo "</table>\n";
									echo "</div>\n";
						        echo "</div>\n";
						      echo "</div>\n";
						    echo "</div>\n";
				    	} else {
				    		echo "<div class='panel-heading'>\n";
						        echo "<h4 class='panel-title'>\n";
						          echo "No Tiene Solicitudes\n";
						        echo "</h4>\n";
						    echo "</div>\n";
				    	}
				    }?>
				    <?php if ($vcdia == $cedula) {
				    	if ($vsimbo > 0) {
						    echo "<div class='panel panel-info'>\n";
						      echo "<div class='panel-heading'>\n";
						        echo "<h4 class='panel-title'>\n";
						          echo "<a data-toggle='collapse' data-parent='#accordion' href='#collapse4'>Aprobación Solicitudes</a>\n";
						        echo "</h4>\n";
						      echo "</div>\n";
						      echo "<div id='collapse4' class='panel-collapse collapse in'>\n";
						        echo "<div class='panel-body'>\n";
						        	echo "<div class='table-responsive'>\n";
										$stid = $conn->Execute($qresudia);
										//oci_execute($stid);	
										echo "<table class='table table-bordered'>\n";
											echo "<tr>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Soliditud</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Grado</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Nombres</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>VoBo Jefe</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha VoBo</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Ver Soliditud</th>\n";
											echo "<th class='text-center text-capitalize active' colspan='2'>Validar Soliditud</th>\n";
											echo "<th class='text-center text-capitalize active' rowspan='2'>Confirmar</th>\n";
											echo "</tr>\n";
											echo "<tr>\n";
											echo "<th class='text-center text-capitalize active'>Aprueba</th>\n\n";
											echo "<th class='text-center text-capitalize active'>Rechaza</th>\n\n";
											echo "</tr>\n";
										    echo "<tr>\n";

										   //while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
										   	while ($row = $stid->FetchNextObject()){
										        echo "<td class='text-center'>",$row->FECHASOL,"</td>";
										        echo "<td class='text-center'>",$row->GRADO,"</td>";
										        echo "<td class='text-center' nowrap>",$row->NOMBRES,"</td>";
										        echo "<td class='text-center'>",$row->APROBOJE,"</td>";
										        echo "<td class='text-center'>",$row->FECHAAPJE,"</td>";
										        $pfce = $row->CEDULARE;
										        echo "<input type='hidden' id='".$pfce."' value='".$row->CEDULARE."'>";
										        echo "<td class='text-center'><button type='button' class='btn btn-info btn-sm' id='impr0' onclick='impr1(".$pfce.")'>
										        	  <span class='glyphicon glyphicon-file'></span></button></td>";
										        echo "<td class='text-center'><input type='radio' name='".$pfce."' value='1'>";
										        echo "<td class='text-center'><input type='radio' name='".$pfce."' value='0'></td>";
										        echo "<td class='text-center'><button type='button' class='btn btn-success btn-sm' id='ok0' onclick='capturar1(".$pfce.")'>
													  <span class='glyphicon glyphicon-ok'></span></button></td>";
										    echo "</tr>\n";
										    }
										echo "</table>\n";
									echo "</div>\n";
						        echo "</div>\n";
						      echo "</div>\n";
						    echo "</div>\n";
						    echo "<div class='panel panel-info'>\n";
						      echo "<div class='panel-heading'>\n";
						        echo "<h4 class='panel-title'>\n";
						          echo "<a data-toggle='collapse' data-parent='#accordion' href='#collapse5'>Historial Soliditudes</b></a>\n";
						       echo "</h4>\n";
						      echo "</div>\n";
						      echo "<div id='collapse5' class='panel-collapse collapse'>\n";
						        echo "<div class='panel-body'>\n";
						        	echo "<div class='table-responsive'>\n";
										$stid = $conn->Execute($todos);
										//oci_execute($stid);	
										echo "<table class='table table-bordered'>\n";
										echo "<tr>\n";
										echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Soliditud</th>\n";
										echo "<th class='text-center text-capitalize active' rowspan='2'>Grado</th>\n";
										echo "<th class='text-center text-capitalize active' rowspan='2'>Nombres</th>\n";
										echo "<th class='text-center text-capitalize active' rowspan='2'>Aprobó Jefe</th>\n";
										echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Apro/Rech</th>\n";
										echo "<th class='text-center text-capitalize active' rowspan='2'>Aprobó DIAPE</th>\n";
										echo "<th class='text-center text-capitalize active' rowspan='2'>Fecha Apr/Rec</th>\n";
										echo "</tr>\n";
										echo "<tr>\n";
										echo "</tr>\n";
									    echo "<tr>\n";

									    //while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
									    while ($row = $stid->FetchNextObject()){
									        echo "<td class='text-center'>",$row->FECHASOL,"</td>";
											echo "<td class='text-center'>",$row->GRADO,"</td>";
											echo "<td class='text-center' nowrap>",$row->NOMBRES,"</td>";
											echo "<td class='text-center'>",$row->APROBOJE,"</td>";
											echo "<td class='text-center'>",$row->FECHAAPJE,"</td>";
											echo "<td class='text-center'>",$row->APROBODIA,"</td>";
											echo "<td class='text-center'>",$row->FECHAAPD,"</td>";
									    echo "</tr>\n";
									    }
								echo "</table>\n";
									echo "</div>\n";
						        echo "</div>\n";
						      echo "</div>\n";
						    echo "</div>\n";
						}else { 
							echo "<div class='panel-heading'>\n";
						        echo "<h4 class='panel-title'>\n";
						          echo "No Tiene Solicitudes\n";
						        echo "</h4>\n";
						    echo "</div>\n";
						}
				    }?>
				</div> 
			</div>
	    </div>
	<div class="col-md-1"></div>
  <script src="bootstrap/js/jquery-1.11.2.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" async="async">

  		function impr(pfce)
	    {
			var ced =document.getElementById(pfce).value;
			document.cookie ='p_ced='+ced;
			window.open("imprpdf.php","Datos del sistema")
		}
		function impr1(pfce)
	    {
			var ced =document.getElementById(pfce).value;
			document.cookie ='p_ced='+ced;
			window.open("imprpdf.php","Datos del sistema")
		}

		function capturar(pfce)
	    {
	        var resultado = "";
	        var ced =document.getElementById(pfce).value;
	        var porNombre=document.getElementsByName(pfce);
	        var ema = 1;
	        for(var i=0;i<porNombre.length;i++)
	        {
	            if(porNombre[i].checked){
	            	resultado=porNombre[i].value;
	            	document.cookie ='p_ced='+ced;
					document.cookie ='p_apr='+resultado;
					document.cookie ='p_ema='+ema;
					iden = <?php echo $cedula?>;
					location.reload("Conexiones/confirmar.php?usr_cc="+iden);
					location.reload(true);
					break;
			    }
	        }
	    }
	    function capturar1(pfce)
	    {
	        var resultado = "";
	        var ced =document.getElementById(pfce).value;
	        var porNombre=document.getElementsByName(pfce);
	        var ema = 2;
	        for(var i=0;i<porNombre.length;i++)
	        {
	            if(porNombre[i].checked){
	            	resultado=porNombre[i].value;
	            	document.cookie ='p_cedu='+ced;
					document.cookie ='p_apro='+resultado;
					document.cookie ='p_ema='+ema;
					iden = <?php echo $cedula?>;
					location.reload("Conexiones/confirmar.php?usr_cc="+iden);
					location.reload(true);
					break;
			    }
	        }
	    }

  </script>
</body>
</html>