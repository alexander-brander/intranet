<?php
/* Función que genera la vista del listado de gestión para un jefe particular */
function vista_jefe($usuario){
    //se carga la identificación del usuario
    $usuario_identification=$usuario->field_user_nit['und'][0]['value'];
    $name_user_view=str_replace('.', '_',$usuario->name);
	$view = new view();
	$view->name = 'cese_militar_autorizar_'.$name_user_view;
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'node';
	$view->human_name = 'Cese Militar Autorizar '.$name_user_view;
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'Cese Militar Autorizar';
	$handler->display->display_options['css_class'] = 'tables-all';
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['access']['type'] = 'role';
	$handler->display->display_options['access']['role'] = array(
	  23 => '23',
	);
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Buscar';
	$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '10';
	$handler->display->display_options['pager']['options']['tags']['first'] = '« primera';
	$handler->display->display_options['pager']['options']['tags']['previous'] = '‹ anterior';
	$handler->display->display_options['pager']['options']['tags']['next'] = 'siguiente ›';
	$handler->display->display_options['pager']['options']['tags']['last'] = 'última »';
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
	  'nid' => 'nid',
	  'php' => 'php',
	  'php_1' => 'php_1',
	  'field_identificacion_cese' => 'field_identificacion_cese',
	  'field_fecha_diligenciamient_cese' => 'field_fecha_diligenciamient_cese',
	  'field_grado_cese' => 'field_grado_cese',
	  'field_tipo_novedad_cese' => 'field_tipo_novedad_cese',
	);
	$handler->display->display_options['style_options']['default'] = '-1';
	$handler->display->display_options['style_options']['info'] = array(
	  'nid' => array(
	    'sortable' => 0,
	    'default_sort_order' => 'asc',
	    'align' => '',
	    'separator' => '',
	    'empty_column' => 0,
	  ),
	  'php' => array(
	    'align' => '',
	    'separator' => '',
	    'empty_column' => 0,
	  ),
	  'php_1' => array(
	    'align' => '',
	    'separator' => '',
	    'empty_column' => 0,
	  ),
	  'field_identificacion_cese' => array(
	    'sortable' => 0,
	    'default_sort_order' => 'asc',
	    'align' => '',
	    'separator' => '',
	    'empty_column' => 0,
	  ),
	  'field_fecha_diligenciamient_cese' => array(
	    'sortable' => 1,
	    'default_sort_order' => 'asc',
	    'align' => '',
	    'separator' => '',
	    'empty_column' => 0,
	  ),
	  'field_grado_cese' => array(
	    'sortable' => 1,
	    'default_sort_order' => 'asc',
	    'align' => '',
	    'separator' => '',
	    'empty_column' => 0,
	  ),
	  'field_tipo_novedad_cese' => array(
	    'sortable' => 1,
	    'default_sort_order' => 'asc',
	    'align' => '',
	    'separator' => '',
	    'empty_column' => 0,
	  ),
	);
	/* No results behavior: Global: PHP */
	$handler->display->display_options['empty']['php']['id'] = 'php';
	$handler->display->display_options['empty']['php']['table'] = 'views';
	$handler->display->display_options['empty']['php']['field'] = 'php';
	$handler->display->display_options['empty']['php']['empty'] = TRUE;
	$handler->display->display_options['empty']['php']['php_output'] = '<h4>NO HAY PETICIONES PARA GESTIONAR</h4>
	';
	/* Campo: Contenido: Título */
	$handler->display->display_options['fields']['title']['id'] = 'title';
	$handler->display->display_options['fields']['title']['table'] = 'node';
	$handler->display->display_options['fields']['title']['field'] = 'title';
	$handler->display->display_options['fields']['title']['label'] = 'Código Hash';
	$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
	/* Campo: Contenido: Grado */
	$handler->display->display_options['fields']['field_grado_cese']['id'] = 'field_grado_cese';
	$handler->display->display_options['fields']['field_grado_cese']['table'] = 'field_data_field_grado_cese';
	$handler->display->display_options['fields']['field_grado_cese']['field'] = 'field_grado_cese';
	$handler->display->display_options['fields']['field_grado_cese']['type'] = 'text_plain';
	/* Campo: Global: PHP */
	$handler->display->display_options['fields']['php']['id'] = 'php';
	$handler->display->display_options['fields']['php']['table'] = 'views';
	$handler->display->display_options['fields']['php']['field'] = 'php';
	$handler->display->display_options['fields']['php']['label'] = 'Apellidos';
	$handler->display->display_options['fields']['php']['use_php_setup'] = 0;
	$handler->display->display_options['fields']['php']['php_output'] = '<?php
	//se carga el nodo del cese
	$node_cese=node_load($row->nid);
	//se almacena el primer apellido
	$primer_apellido=$node_cese->field_primer_apellido_cese[\'und\'][0][\'value\'];
	//se almacena segundo apellido
	$segundo_apellido=$node_cese->field_segundo_apellido_cese[\'und\'][0][\'value\'];
	//se pintan los valores unidos
	echo $primer_apellido.\'  \'.$segundo_apellido;
	?>';
	$handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
	$handler->display->display_options['fields']['php']['php_click_sortable'] = '';
	/* Campo: Global: PHP */
	$handler->display->display_options['fields']['php_1']['id'] = 'php_1';
	$handler->display->display_options['fields']['php_1']['table'] = 'views';
	$handler->display->display_options['fields']['php_1']['field'] = 'php';
	$handler->display->display_options['fields']['php_1']['label'] = 'Nombres';
	$handler->display->display_options['fields']['php_1']['use_php_setup'] = 0;
	$handler->display->display_options['fields']['php_1']['php_output'] = '<?php
	//se carga en nodo de cese
	$cese_node=node_load($row->nid);
	//se almacena el primer nombre
	$primer_nombre=isset($cese_node->field_primer_nombre_cese[\'und\'][0][\'value\'])?$cese_node->field_primer_nombre_cese[\'und\'][0][\'value\']:\' \';
	//se almacena el segundo nombre
	$segundo_nombre=isset($cese_node->field_segundo_nombre_cese[\'und\'][0][\'value\'])?$cese_node->field_segundo_nombre_cese[\'und\'][0][\'value\']:\' \';
	echo $primer_nombre.\'  \'.$segundo_nombre;
	?>';
	$handler->display->display_options['fields']['php_1']['use_php_click_sortable'] = '0';
	$handler->display->display_options['fields']['php_1']['php_click_sortable'] = '';
	/* Campo: Contenido: Número Cédula Ciudadanía */
	$handler->display->display_options['fields']['field_identificacion_cese']['id'] = 'field_identificacion_cese';
	$handler->display->display_options['fields']['field_identificacion_cese']['table'] = 'field_data_field_identificacion_cese';
	$handler->display->display_options['fields']['field_identificacion_cese']['field'] = 'field_identificacion_cese';
	$handler->display->display_options['fields']['field_identificacion_cese']['label'] = 'N° Identificación';
	$handler->display->display_options['fields']['field_identificacion_cese']['settings'] = array(
	  'thousand_separator' => ' ',
	  'prefix_suffix' => 1,
	);
	/* Campo: Contenido: Fecha Diligenciamiento */
	$handler->display->display_options['fields']['field_fecha_diligenciamient_cese']['id'] = 'field_fecha_diligenciamient_cese';
	$handler->display->display_options['fields']['field_fecha_diligenciamient_cese']['table'] = 'field_data_field_fecha_diligenciamient_cese';
	$handler->display->display_options['fields']['field_fecha_diligenciamient_cese']['field'] = 'field_fecha_diligenciamient_cese';
	$handler->display->display_options['fields']['field_fecha_diligenciamient_cese']['settings'] = array(
	  'format_type' => 'short',
	  'fromto' => 'both',
	  'multiple_number' => '',
	  'multiple_from' => '',
	  'multiple_to' => '',
	);
	/* Campo: Contenido: Tipo Novedad */
	$handler->display->display_options['fields']['field_tipo_novedad_cese']['id'] = 'field_tipo_novedad_cese';
	$handler->display->display_options['fields']['field_tipo_novedad_cese']['table'] = 'field_data_field_tipo_novedad_cese';
	$handler->display->display_options['fields']['field_tipo_novedad_cese']['field'] = 'field_tipo_novedad_cese';
	$handler->display->display_options['fields']['field_tipo_novedad_cese']['label'] = 'Novedad';
	$handler->display->display_options['fields']['field_tipo_novedad_cese']['type'] = 'text_plain';
	/* Campo: Contenido: Nid */
	$handler->display->display_options['fields']['nid']['id'] = 'nid';
	$handler->display->display_options['fields']['nid']['table'] = 'node';
	$handler->display->display_options['fields']['nid']['field'] = 'nid';
	$handler->display->display_options['fields']['nid']['label'] = 'Administrar Solicitud';
	$handler->display->display_options['fields']['nid']['alter']['alter_text'] = TRUE;
	$handler->display->display_options['fields']['nid']['alter']['text'] = '<a href=\'/node/[nid]/edit\'> Aprobar</a>';
	$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
	/* Sort criterion: Contenido: Post date */
	$handler->display->display_options['sorts']['created']['id'] = 'created';
	$handler->display->display_options['sorts']['created']['table'] = 'node';
	$handler->display->display_options['sorts']['created']['field'] = 'created';
	$handler->display->display_options['sorts']['created']['order'] = 'DESC';
	/* Filter criterion: Contenido: Publicado */
	$handler->display->display_options['filters']['status']['id'] = 'status';
	$handler->display->display_options['filters']['status']['table'] = 'node';
	$handler->display->display_options['filters']['status']['field'] = 'status';
	$handler->display->display_options['filters']['status']['value'] = 1;
	$handler->display->display_options['filters']['status']['group'] = 1;
	$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
	/* Filter criterion: Contenido: Tipo */
	$handler->display->display_options['filters']['type']['id'] = 'type';
	$handler->display->display_options['filters']['type']['table'] = 'node';
	$handler->display->display_options['filters']['type']['field'] = 'type';
	$handler->display->display_options['filters']['type']['value'] = array(
	  'cese_militar' => 'cese_militar',
	);
	/* Filter criterion: Contenido: Estado (field_estado_cese) */
	$handler->display->display_options['filters']['field_estado_cese_tid']['id'] = 'field_estado_cese_tid';
	$handler->display->display_options['filters']['field_estado_cese_tid']['table'] = 'field_data_field_estado_cese';
	$handler->display->display_options['filters']['field_estado_cese_tid']['field'] = 'field_estado_cese_tid';
	$handler->display->display_options['filters']['field_estado_cese_tid']['value'] = array(
	  37 => '37',
	);
	$handler->display->display_options['filters']['field_estado_cese_tid']['type'] = 'select';
	$handler->display->display_options['filters']['field_estado_cese_tid']['vocabulary'] = 'estado_cese_militar';
	/* Filter criterion: Contenido: Título */
	$handler->display->display_options['filters']['title']['id'] = 'title';
	$handler->display->display_options['filters']['title']['table'] = 'node';
	$handler->display->display_options['filters']['title']['field'] = 'title';
	$handler->display->display_options['filters']['title']['exposed'] = TRUE;
	$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
	$handler->display->display_options['filters']['title']['expose']['label'] = 'Código Hash';
	$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
	$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
	$handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
	  2 => '2',
	  1 => 0,
	  10 => 0,
	  9 => 0,
	  8 => 0,
	  3 => 0,
	  4 => 0,
	  5 => 0,
	  6 => 0,
	  7 => 0,
	  11 => 0,
	  12 => 0,
	  13 => 0,
	  14 => 0,
	  15 => 0,
	  17 => 0,
	  18 => 0,
	  19 => 0,
	  20 => 0,
	  21 => 0,
	  22 => 0,
	  23 => 0,
	  24 => 0,
	  25 => 0,
	  26 => 0,
	);
	/* Filter criterion: Contenido: Número Cédula Ciudadanía (field_identificacion_cese) */
	$handler->display->display_options['filters']['field_identificacion_cese_value']['id'] = 'field_identificacion_cese_value';
	$handler->display->display_options['filters']['field_identificacion_cese_value']['table'] = 'field_data_field_identificacion_cese';
	$handler->display->display_options['filters']['field_identificacion_cese_value']['field'] = 'field_identificacion_cese_value';
	$handler->display->display_options['filters']['field_identificacion_cese_value']['exposed'] = TRUE;
	$handler->display->display_options['filters']['field_identificacion_cese_value']['expose']['operator_id'] = 'field_identificacion_cese_value_op';
	$handler->display->display_options['filters']['field_identificacion_cese_value']['expose']['label'] = 'Identificación';
	$handler->display->display_options['filters']['field_identificacion_cese_value']['expose']['operator'] = 'field_identificacion_cese_value_op';
	$handler->display->display_options['filters']['field_identificacion_cese_value']['expose']['identifier'] = 'field_identificacion_cese_value';
	$handler->display->display_options['filters']['field_identificacion_cese_value']['expose']['remember_roles'] = array(
	  2 => '2',
	  1 => 0,
	  10 => 0,
	  9 => 0,
	  8 => 0,
	  3 => 0,
	  4 => 0,
	  5 => 0,
	  6 => 0,
	  7 => 0,
	  11 => 0,
	  12 => 0,
	  13 => 0,
	  14 => 0,
	  15 => 0,
	  17 => 0,
	  18 => 0,
	  19 => 0,
	  20 => 0,
	  21 => 0,
	  22 => 0,
	  23 => 0,
	  24 => 0,
	  25 => 0,
	  26 => 0,
	);
	/* Filter criterion: Contenido: Identificación Jefe (field_identification_jefe_cese) */
	$handler->display->display_options['filters']['field_identification_jefe_cese_value']['id'] = 'field_identification_jefe_cese_value';
	$handler->display->display_options['filters']['field_identification_jefe_cese_value']['table'] = 'field_data_field_identification_jefe_cese';
	$handler->display->display_options['filters']['field_identification_jefe_cese_value']['field'] = 'field_identification_jefe_cese_value';
	$handler->display->display_options['filters']['field_identification_jefe_cese_value']['value'] = $usuario_identification;

	/* Display: Page */
	$handler = $view->new_display('page', 'Page', 'page');
	$handler->display->display_options['path'] = 'cese-militar-autorizar-'.$name_user_view;
	$translatables['cese_militar_autorizar_'] = array(
	  t('Master'),
	  t('Cese Militar Autorizar'),
	  t('more'),
	  t('Buscar'),
	  t('Reiniciar'),
	  t('Sort by'),
	  t('Asc'),
	  t('Desc'),
	  t('Items per page'),
	  t('- All -'),
	  t('Offset'),
	  t('« primera'),
	  t('‹ anterior'),
	  t('siguiente ›'),
	  t('última »'),
	  t('Código Hash'),
	  t('Apellidos'),
	  t('Nombres'),
	  t('N° Identificación'),
	  t('Fecha Diligenciamiento'),
	  t('Grado'),
	  t('Novedad'),
	  t('Administrar Solicitud'),
	  t('<a href=\'/node/[nid]/edit\'> Aprobar</a>'),
	  t('Identificación'),
	  t('Page'),
	);
	return $view;
}
?>