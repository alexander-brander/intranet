$data = array(
  'bundles' => array(
    'partida_usuarios' => (object) array(
      'type' => 'partida_usuarios',
      'name' => 'Partida Usuarios',
      'base' => 'node_content',
      'module' => 'node',
      'description' => 'Contenido en el cual se registran las partidas para el tramite de vestuario',
      'help' => '',
      'has_title' => '1',
      'title_label' => 'Identificación',
      'custom' => '1',
      'modified' => '1',
      'locked' => '0',
      'disabled' => '0',
      'orig_type' => 'partida_usuarios',
      'disabled_changed' => FALSE,
      'bc_entity_type' => 'node',
    ),
  ),
  'fields' => array(
    'field_anio_partida' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_anio_partida' => array(
                'value' => 'field_anio_partida_value',
                'format' => 'field_anio_partida_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_anio_partida' => array(
                'value' => 'field_anio_partida_value',
                'format' => 'field_anio_partida_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_anio_partida',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'partida_usuarios',
        ),
      ),
    ),
    'field_apellidos_partida' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_apellidos_partida' => array(
                'value' => 'field_apellidos_partida_value',
                'format' => 'field_apellidos_partida_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_apellidos_partida' => array(
                'value' => 'field_apellidos_partida_value',
                'format' => 'field_apellidos_partida_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_apellidos_partida',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'partida_usuarios',
        ),
      ),
    ),
    'field_estado_partida' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_estado_partida' => array(
                'value' => 'field_estado_partida_value',
                'format' => 'field_estado_partida_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_estado_partida' => array(
                'value' => 'field_estado_partida_value',
                'format' => 'field_estado_partida_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_estado_partida',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'partida_usuarios',
        ),
      ),
    ),
    'field_grado_partida' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_grado_partida' => array(
                'value' => 'field_grado_partida_value',
                'format' => 'field_grado_partida_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_grado_partida' => array(
                'value' => 'field_grado_partida_value',
                'format' => 'field_grado_partida_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_grado_partida',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'partida_usuarios',
        ),
      ),
    ),
    'field_nombres_partida' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_nombres_partida' => array(
                'value' => 'field_nombres_partida_value',
                'format' => 'field_nombres_partida_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_nombres_partida' => array(
                'value' => 'field_nombres_partida_value',
                'format' => 'field_nombres_partida_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_nombres_partida',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'partida_usuarios',
        ),
      ),
    ),
    'field_saldo_partida' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_saldo_partida' => array(
                'value' => 'field_saldo_partida_value',
                'format' => 'field_saldo_partida_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_saldo_partida' => array(
                'value' => 'field_saldo_partida_value',
                'format' => 'field_saldo_partida_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_saldo_partida',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'partida_usuarios',
        ),
      ),
    ),
  ),
  'instances' => array(
    'field_anio_partida' => array(
      0 => array(
        'label' => 'Año',
        'widget' => array(
          'weight' => '0',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 0,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_anio_partida',
        'entity_type' => 'node',
        'bundle' => 'partida_usuarios',
        'deleted' => '0',
      ),
    ),
    'field_apellidos_partida' => array(
      0 => array(
        'label' => 'Apellidos',
        'widget' => array(
          'weight' => '4',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 3,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_apellidos_partida',
        'entity_type' => 'node',
        'bundle' => 'partida_usuarios',
        'deleted' => '0',
      ),
    ),
    'field_estado_partida' => array(
      0 => array(
        'label' => 'Estado',
        'widget' => array(
          'weight' => '1',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 1,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_estado_partida',
        'entity_type' => 'node',
        'bundle' => 'partida_usuarios',
        'deleted' => '0',
      ),
    ),
    'field_grado_partida' => array(
      0 => array(
        'label' => 'Grado',
        'widget' => array(
          'weight' => '2',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 2,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_grado_partida',
        'entity_type' => 'node',
        'bundle' => 'partida_usuarios',
        'deleted' => '0',
      ),
    ),
    'field_nombres_partida' => array(
      0 => array(
        'label' => 'Nombres',
        'widget' => array(
          'weight' => '5',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 4,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_nombres_partida',
        'entity_type' => 'node',
        'bundle' => 'partida_usuarios',
        'deleted' => '0',
      ),
    ),
    'field_saldo_partida' => array(
      0 => array(
        'label' => 'Saldo Partida',
        'widget' => array(
          'weight' => '6',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 5,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_saldo_partida',
        'entity_type' => 'node',
        'bundle' => 'partida_usuarios',
        'deleted' => '0',
      ),
    ),
  ),
);