$data = array(
  'bundles' => array(
    'solicitud_vestuario' => (object) array(
      'type' => 'solicitud_vestuario',
      'name' => 'Solicitud Vestuario',
      'base' => 'node_content',
      'module' => 'node',
      'description' => '',
      'help' => '',
      'has_title' => '1',
      'title_label' => 'Ticket',
      'custom' => '1',
      'modified' => '1',
      'locked' => '0',
      'disabled' => '0',
      'orig_type' => 'solicitud_vestuario',
      'disabled_changed' => FALSE,
      'bc_entity_type' => 'node',
    ),
  ),
  'fields' => array(
    'field_identificacion_sves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_identificacion_sves' => array(
                'value' => 'field_identificacion_sves_value',
                'format' => 'field_identificacion_sves_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_identificacion_sves' => array(
                'value' => 'field_identificacion_sves_value',
                'format' => 'field_identificacion_sves_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_identificacion_sves',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'solicitud_vestuario',
        ),
      ),
    ),
    'field_nombre_solicitud_sves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_nombre_solicitud_sves' => array(
                'value' => 'field_nombre_solicitud_sves_value',
                'format' => 'field_nombre_solicitud_sves_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_nombre_solicitud_sves' => array(
                'value' => 'field_nombre_solicitud_sves_value',
                'format' => 'field_nombre_solicitud_sves_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_nombre_solicitud_sves',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'solicitud_vestuario',
        ),
      ),
    ),
    'field_unidad_para_reclamar_sves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'almacenes_vestuario',
            'parent' => '0',
          ),
        ),
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_unidad_para_reclamar_sves' => array(
                'tid' => 'field_unidad_para_reclamar_sves_tid',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_unidad_para_reclamar_sves' => array(
                'tid' => 'field_unidad_para_reclamar_sves_tid',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'tid' => array(
          'table' => 'taxonomy_term_data',
          'columns' => array(
            'tid' => 'tid',
          ),
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'field_name' => 'field_unidad_para_reclamar_sves',
      'type' => 'taxonomy_term_reference',
      'module' => 'taxonomy',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'solicitud_vestuario',
        ),
      ),
    ),
    'field_valor_deducido_sves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_valor_deducido_sves' => array(
                'value' => 'field_valor_deducido_sves_value',
                'format' => 'field_valor_deducido_sves_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_valor_deducido_sves' => array(
                'value' => 'field_valor_deducido_sves_value',
                'format' => 'field_valor_deducido_sves_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_permissions' => array(
        'type' => '0',
      ),
      'field_name' => 'field_valor_deducido_sves',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'solicitud_vestuario',
        ),
      ),
    ),
    'field_valor_disponible_sves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_valor_disponible_sves' => array(
                'value' => 'field_valor_disponible_sves_value',
                'format' => 'field_valor_disponible_sves_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_valor_disponible_sves' => array(
                'value' => 'field_valor_disponible_sves_value',
                'format' => 'field_valor_disponible_sves_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_permissions' => array(
        'type' => '2',
      ),
      'field_name' => 'field_valor_disponible_sves',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'solicitud_vestuario',
        ),
      ),
    ),
    'field_valor_partida_sves' => array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_valor_partida_sves' => array(
                'value' => 'field_valor_partida_sves_value',
                'format' => 'field_valor_partida_sves_format',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_valor_partida_sves' => array(
                'value' => 'field_valor_partida_sves_value',
                'format' => 'field_valor_partida_sves_format',
              ),
            ),
          ),
        ),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array(
            'format' => 'format',
          ),
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'field_valor_partida_sves',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'bundles' => array(
        'node' => array(
          0 => 'solicitud_vestuario',
        ),
      ),
    ),
  ),
  'instances' => array(
    'field_identificacion_sves' => array(
      0 => array(
        'label' => 'Identificación',
        'widget' => array(
          'weight' => '-3',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 1,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_identificacion_sves',
        'entity_type' => 'node',
        'bundle' => 'solicitud_vestuario',
        'deleted' => '0',
      ),
    ),
    'field_nombre_solicitud_sves' => array(
      0 => array(
        'label' => 'Nombre Solicitud',
        'widget' => array(
          'weight' => '2',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 6,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_nombre_solicitud_sves',
        'entity_type' => 'node',
        'bundle' => 'solicitud_vestuario',
        'deleted' => '0',
      ),
    ),
    'field_unidad_para_reclamar_sves' => array(
      0 => array(
        'label' => 'Unidad Para Reclamar',
        'widget' => array(
          'weight' => '-4',
          'type' => 'options_select',
          'module' => 'options',
          'active' => 1,
          'settings' => array(),
        ),
        'settings' => array(
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'taxonomy_term_reference_link',
            'settings' => array(),
            'module' => 'taxonomy',
            'weight' => 0,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 1,
        'description' => 'En este campo puede elegir la unidad en la cual puede recoger su vestuario seleccionado.',
        'default_value' => NULL,
        'field_name' => 'field_unidad_para_reclamar_sves',
        'entity_type' => 'node',
        'bundle' => 'solicitud_vestuario',
        'deleted' => '0',
      ),
    ),
    'field_valor_deducido_sves' => array(
      0 => array(
        'label' => 'Valor Deducido:',
        'widget' => array(
          'weight' => '1',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '20',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 5,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => array(
          0 => array(
            'value' => '0',
          ),
        ),
        'field_name' => 'field_valor_deducido_sves',
        'entity_type' => 'node',
        'bundle' => 'solicitud_vestuario',
        'deleted' => '0',
      ),
    ),
    'field_valor_disponible_sves' => array(
      0 => array(
        'label' => 'Valor Disponible:',
        'widget' => array(
          'weight' => '0',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '20',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 4,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_valor_disponible_sves',
        'entity_type' => 'node',
        'bundle' => 'solicitud_vestuario',
        'deleted' => '0',
      ),
    ),
    'field_valor_partida_sves' => array(
      0 => array(
        'label' => 'Valor Partida',
        'widget' => array(
          'weight' => '3',
          'type' => 'text_textfield',
          'module' => 'text',
          'active' => 1,
          'settings' => array(
            'size' => '30',
          ),
        ),
        'settings' => array(
          'text_processing' => '0',
          'mediafront' => array(
            'field_type' => '0',
            'media_type' => 'media',
            'preview' => '0',
            'thumbnail' => '0',
            'custom' => '',
          ),
          'user_register_form' => FALSE,
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'text_default',
            'settings' => array(),
            'module' => 'text',
            'weight' => 7,
          ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
          ),
        ),
        'required' => 0,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_valor_partida_sves',
        'entity_type' => 'node',
        'bundle' => 'solicitud_vestuario',
        'deleted' => '0',
      ),
    ),
  ),
);