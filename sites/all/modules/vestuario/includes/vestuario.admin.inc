<?php

/**
 * @file
 * Administration callbacks of vestuario module
 */

/**
 * Defines the settings form
 */
//Definimos los elementos del formulario en un array
function vestuario_settings_form() {
    $form = array();

    
    $form['webservice_info'] = array(
      '#type'   => 'fieldset',
      '#title'  => t('Webservice information'),
      '#prefix' => '<div id="webservice_info-wrapper">',
      '#suffix' => '</div>',
      '#tree'   => FALSE,  //Para que guarde las variables que estan por dentro de manera independiente y no como un arreglo
    );
      $form['webservice_info']['vestuario_webservice_url'] = array(
          '#title' => t('Webservice URL'),
          '#type' => 'textfield',
          '#default_value' => variable_get('vestuario_webservice_url', 'http://www.armada.mil.co/arc_ws_siath/siathWS.php?wsdl'),
          '#required' => TRUE,
      );
      $form['webservice_info']['vestuario_webservice_key'] = array(
          '#title' => t('Webservice Key'),
          '#type' => 'textfield',
          '#default_value' => variable_get('vestuario_webservice_key', 'alksdjfwoieur'),
          '#required' => TRUE,
      );
    $form['clothes_filters_file'] = array(
        '#type' => 'file',
        '#title' => t('Archivo de texto plano para filtro del vestuario.'),
        '#description' => t('Este archivo debe contener la información de los filtros de cada una de las prendas del vestuario relacionadas con todos los grados de la Armada Nacional.'),
        '#size' => 40,
    );
    $form['inventory_file'] = array(
        '#type' => 'file',
        '#title' => t('Archivo de texto plano con el inventario de vestuario.'),
        '#description' => t('Este archivo debe contener el inventario de todas las prendas de la Armada Nacional.'),
        '#size' => 40,
    );
    $form['solicitud_dia'] = array(
            '#title' => t('Solicitudes por día'),
            '#type' => 'textfield',
            '#description'=>'Ingrese el número de solicitudes que se pueden realizar por día',
            '#default_value' => variable_get('solicitud_dia', '20'),
            '#required' => TRUE,
    );

//    //Necesario para que el campo file funcione.
    $form['#attributes'] = array('enctype' => "multipart/form-data");
    $form['#submit'][] = 'vestuario_settings_form_submit';

    return system_settings_form($form);
}

/**
 * Validates the settings form
 */
//Validación manual de los datos del formulario
function vestuario_settings_form_validate($form, &$form_state) {
    // Si un archivo de filtros se subió, se procesa.
    if (isset($_FILES['files']) && is_uploaded_file($_FILES['files']['tmp_name']['clothes_filters_file'])) {
        // Valida la extención del archivo
        $file_extensions = array('text/csv','application/vnd.ms-excel','application/octet-stream');
        if (!in_array($_FILES['files']['type']['clothes_filters_file'], $file_extensions)) {
            form_set_error('clothes_filters_file', t('El tipo de archivo seleccionado no es permitido.'));
            return;
        }
        // Se verifica que el nombre del archivo corresponda con el necesitado por el módulo
        if ($_FILES['files']['name']['clothes_filters_file'] != 'clothes_filter.csv') {
            form_set_error('clothes_filters_file', t('El nombre del archivo de filtros debe ser "clothes_filter.csv".'));
            return;
        }
        // Se configura el directorio en donde se guardará el archivo y se verifica
        $files_dir = file_stream_wrapper_get_instance_by_uri('public://')->getDirectoryPath();
        $directorio = $files_dir . '/vestuario';
        if (file_prepare_directory($directorio, FILE_CREATE_DIRECTORY)) {
            // Se intenta guardar el archivo subido
            $validators = array('file_validate_extensions' => array('csv'));
            $file = file_save_upload('clothes_filters_file', $validators, 'public://vestuario', FILE_EXISTS_REPLACE);
            // se muestra un error si el archivo no se guardó correctamente
            if (!$file) {
                form_set_error('clothes_filters_file', t('Error subiendo el archivo filters.'));
                return;
            }
            // Se envía la información del archivo dentro de los datos del formulario para ser procesado en el submit
            $form_state['storage']['clothes_filters_file'] = $file;
        }
    }

    // Si un archivo de inventario se subió, se procesa.
    if (isset($_FILES['files']) && is_uploaded_file($_FILES['files']['tmp_name']['inventory_file'])) {
        // Valida la extención del archivo
        $file_extensions = array('text/csv','application/vnd.ms-excel','application/octet-stream');
        if (!in_array($_FILES['files']['type']['inventory_file'], $file_extensions)) {
            form_set_error('inventory_file', t('El tipo de archivo seleccionado no es permitido.'));
            return;
        }
        // Se verifica que el nombre del archivo corresponda con el necesitado por el módulo
        if ($_FILES['files']['name']['inventory_file'] != 'inventory.csv') {
            form_set_error('inventory_file', t('El nombre del archivo de filtros debe ser "inventory.csv".'));
            return;
        }
        // Se configura el directorio en donde se guardará el archivo y se verifica
        $files_dir = file_stream_wrapper_get_instance_by_uri('public://')->getDirectoryPath();
        $directorio = $files_dir . '/vestuario';
        if (file_prepare_directory($directorio, FILE_CREATE_DIRECTORY)) {
            // Se intenta guardar el archivo subido
            $validators = array('file_validate_extensions' => array('csv'));
            $file = file_save_upload('inventory_file', $validators, 'public://vestuario', FILE_EXISTS_REPLACE);
            // se muestra un error si el archivo no se guardó correctamente
            if (!$file) {
                form_set_error('inventory_file', t('Error subiendo el archivo inventory.'));
            }
            // Se envía la información del archivo dentro de los datos del formulario para ser procesado en el submit
            $form_state['storage']['inventory_file'] = $file;
        }
    }
}

/**
 * Submit the settings form
 */
//Procesamiento del formulario de administración del módulo
function vestuario_settings_form_submit($form, &$form_state) {
    // Si se procesó correctamente el archivo de filtros
    if (!empty($form_state['storage']['clothes_filters_file'])) {
        $clothes_filters_file = $form_state['storage']['clothes_filters_file'];
        unset($form_state['storage']['clothes_filters_file']);
        drupal_set_message('Se ha guardado correctamente el archivo de filtros para vestuario.');
    }
    // Si se procesó correctamente el archivo de inventario
    if (!empty($form_state['storage']['inventory_file'])) {
        $inventory_file = $form_state['storage']['inventory_file'];
        unset($form_state['storage']['inventory_file']);
        drupal_set_message('Se ha guardado correctamente el archivo del inventario de vestuario.');
        vestuario_load_inventory();
    }
}

/**
 * Esta función crea un arreglo con la invormación del archivo inventory.csv.
 *
 * @return array
 */
function vestuario_get_clothes_inventory() {
    // Se carga el archivo csv con la información del inventario
    $files_dir = file_stream_wrapper_get_instance_by_uri('public://')->getDirectoryPath();
    $path = $files_dir . '/vestuario';
    $file = fopen($path . '/inventory.csv', 'r');
    // Se obtiene la primera fila del archivo como las keys secundarias para cada prenda
    $secondary_keys = fgetcsv($file);

    $inventory = array();
    // Se recorre todo el archivo
    while (($line = fgetcsv($file)) !== FALSE) {
        // Si la fila tiene el campo de referencia
        if (!empty($line[0])) {
            // Se combinan las llaves secundarias con la información de la línea
            $inventory[] = array_combine($secondary_keys, $line);
        }
    }
    fclose($file);
    return $inventory;
}

/**
 * Esta función carga el contenido del archivo plano de inventario como nodos de tipo Vestuario (prenda)
 */
function vestuario_load_inventory() {
    global $user;
    // Se obtienen todas las prendas deñl inventario
    $inventory = vestuario_get_clothes_inventory();
    // Se obtienen las llaves de los campos del inventario
    $keys = array_keys($inventory[0]);
    // Se eliminan los elementos en el inventario creados anteriormente
    $query = "SELECT nid FROM {node} WHERE type = 'inventario_vestuario';";
    $result = db_query($query);
    foreach ($result as $value) {
        $nid = $value->nid;
        node_delete($nid);
        $node_exists = node_load($nid);
    }
    drupal_get_messages();
    //Se crea un nodo del tipo de contenido 'inventario_vestuario' para cada uno de los elementos del inventario si este no existe.
    foreach ($inventory as $prenda) {
        $query = "SELECT nid FROM {node} LEFT JOIN field_data_field_vest_clothes_reference ON nid = entity_id WHERE field_vest_clothes_reference_value = '".$prenda[$keys[0]]."'";
        $result = db_query($query);
        if ($result->rowCount() <= 0) {
            // Si la prenda no existe en el inventario
            $node = new stdClass();
            $node->type = 'inventario_vestuario';
            $node->uid = $user->uid;
            $node->name = $user->name;
            $node->title = $prenda[$keys[1]];
            $node->status = 1; // published
            $node->promote = 0;

            $node->field_vest_clothes_reference['und'][0]['value'] = str_replace(",", "", $prenda[$keys[0]]);
            $node->field_vest_clothes_inventory['und'][0]['value'] = str_replace(",", "", $prenda[$keys[2]]);
            node_save($node);
        } else {
            // Si la prenda ya existe en el inventario
            $node = node_load($nid);
            $node->title = $prenda[$keys[1]];
            $node->status = 1; // published
            $node->promote = 0;
             
            $node->field_vest_clothes_reference['und'][0]['value'] = str_replace(",", "", $prenda[$keys[0]]);
            $node->field_vest_clothes_inventory['und'][0]['value'] = str_replace(",", "", $prenda[$keys[2]]);
            node_save($node);
        }
    }
}