<?php
/*
 * funcion que genera el formulario de autenticación al módulo de evaluación caem
*/
function caz_siath_auth_form($form, &$form_state){
	unset($_SESSION["caz"]);
	$identificacion_select = isset($form_state["values"]["authen_caz"]['identification_caz']) ? $form_state["values"]["authen_caz"]['identification_caz']:'';
	$codigo_militar = isset($form_state["values"]["authen_caz"]['military_code_caz']) ? $form_state["values"]["authen_caz"]['military_code_caz']:'';
	//contenedor del formulario de autenticacion por codigo militar
	$form["authen_caz"] = array(
			'#type' => 'fieldset',
			'#title' => t('Paso 1 - Autenticación'),
			'#collapsible' => TRUE,
			'#collapsed' => false,
			'#tree' => TRUE, // Don't forget to set #tree!
	);
	//Campo donde se registra en numero de identificacion del usuario
	$form["authen_caz"]['identification_caz'] = array(
			'#type' => 'textfield',
			'#title' => t('Número de Identificación'),
			'#description'=> t('Por favor ingrese su número de identificación'),
			'#default_value' => $identificacion_select,
			'#size' => 30,
			'#maxlength' => 128,
			'#required' => TRUE,
	);
	//campo donde se almacena el codigo militar del usuario
	$form["authen_caz"]['military_code_caz'] = array(
			'#type' => 'textfield',
			'#title' => t('Código'),
			'#description' => t('Por favor ingrese su código militar.'),
			'#default_value' => $codigo_militar,
			'#size' => 30,
			'#maxlength' => 128,
			'#required' => TRUE,
	);
	//submit del formulario
	$form["submit"] = array(
			'#type' => 'submit',
			'#value' => t('Enviar'),
	);
	return $form;
}
/*
 * Submit del formulario donde se validan los datos ingresados
*/
function caz_siath_auth_form_submit($form, &$form_state){
	//se almacenan los valores
	$form_values = $form_state["values"];
	$message = "";
	//variable que contiene la llave de conexion al webservice
	$key = variable_get('caz_siath_ws_key', '');
	//variable que contiene la url de conexion
	$url = variable_get('caz_siath_ws_url', '');
	//se valida la accion del usuario
	if ($form_values['op'] == "Enviar") {
		try {
			/* Hace la validación a través del webservice */
			$client = new SoapClient($url, array('cache.wsdl' => WSDL_CACHE_NONE));
			//resultado del metodo de validación de usuario
			$result = $client->validateUser($form_values["authen_caz"]["identification_caz"], $form_values["authen_caz"]["military_code_caz"], $key, '');
			//Se almacena el correo
			$_SESSION["caz"]["mail"]=isset($result["email"])? $result["email"]:'';
			//Se carga el grado del usuario
			$_SESSION["caz"]["active"]=isset($result["active"])?$result["active"]:'';
		} //Si entra por la excepcion se le presenta al usuario un mensaje para que lo intente de nuevo
		catch (Exception $e) {
			drupal_get_messages();
			drupal_set_message('Se ha presentado un error, por favor intentelo nuevamente (' . $e->getMessage() . ").\n", 'error');
			return;
		}
		//Si la validación es correcta se pasa a la validación de preguntas
		if (!empty($result) && $result["validated"] == '1'){
			//Se almacena el valor de la cedula del usuario
			$_SESSION["caz"]["identification"] = $form_values["authen_caz"]["identification_caz"];
			//Se almacena el valor de codigo militar
			$_SESSION["caz"]["military_code"] = $form_values["authen_caz"]["military_code_caz"];
			//Variable que valida la autenticacion con un valor boleano en true
			$_SESSION["caz"]["auth_caz"] = true;
			$_SESSION["caz"]["time"] = strtotime("now");
			//Mensaje donde se le notifica al usuario que debe validar las preguntas
			$message = "Se ha identificado correctamente. Por favor responda las siguientes preguntas para validar su identidad.";
			drupal_set_message(t($message));
			//Redirección a las preguntas de validación de identidad
			$form_state['redirect'] = "createAccountZimbra/siath/identity_auth";
		}
		//si la validacion el vacia o diferente a 1 se le informa un usuario
		else {
			$error = true;
			$message = "Se ha presentado un error en la validación de sus datos";
			drupal_set_message(t($message), 'error');
		}
	
	}
}
//Funcion que valida si la autenticacion es correcta
function caz_siath_identity(){
	//validación del tiempo de autenticación y se valida que la autenticación sea correcta
	if (caz_validate_session() && $_SESSION["caz"]["auth_caz"] == '1') {
		//retorna al formulario donde se presentan las preguntas
		return drupal_get_form("caz_identity_form");
	}
	else {
		//Se valida que el usuario primero pase por la autenticación
		drupal_set_message(t("Para acceder a la validación de identidad debe seleccionar el tipo de autenticación."), "warning");
		drupal_goto("createAccountZimbra/authentication/siath");
	}
}
//valida el tiempo de validación
function caz_validate_session() {
	$result = false;
	//Se valida que la variable de tiempo este definida
	if (isset($_SESSION["caz"]["time"])) {
		//se suma el tiempo determinado para la utenticación
		$case_session_max_time = $_SESSION["caz"]["time"] + (5 * 60);
		//variable con el tiempo sin alterar
		$now = strtotime('now');
		//se valida el tiempo sin alterar
		if ($now > $case_session_max_time) {
			//si es mayor el tiempo sin alterar se realiza un unset para  todas las variables de sesion
			unset($_SESSION["caz"]);
		}
		else {
			//Se asiga el nuevo tiempo a la variable
			$_SESSION["caz"]["time"] = strtotime('now');
			//Se cambia la a verdadero el valor de result
			$result = true;
		}
	} else {
		//Si no esta definido el tiempo se realiza un unset para todas las variables de sesion
		unset($_SESSION["caz"]);
	}
	//Se retornar el valor del result
	return $result;
}
//funcion que contiene el formulario de las preguntas y las respuestas de autenticación
function caz_identity_form($form, &$form_state){
	//Se define el arreglo form
	$form = array();
	//Si no se han cargado las preguntas se cargan en este punto
	if (!isset($_SESSION["caz"]['questions'])) {
		//se almacenan las preguntas
		$_SESSION["caz"]['questions'] = caz_get_questions($_SESSION["caz"]["identification"], 0);
	}
	// Si aun no se ha definido un paso se inicializa en el paso 1 de la validación
	if (!isset($_SESSION["caz"]['step_index'])) {
		$_SESSION["caz"]['step_index'] = 1;
	}
	// Se obtiene una copia local del paso actual
	$step = $_SESSION["caz"]['step_index'];
	// Obtiene el indice de la pregunta actual
	$index = $step - 1;
	// Obtiene una copia de la definición de la pregunta actual
	$question = $_SESSION["caz"]['questions'][$index];
	// @var $id_question string
	$id_question = $question['question']['questionId'];
	// @var $question_text string
	$question_text = $question['question']['questionText'];
	// Elimina la información de la pregunta del arreglo para obtene las respuestas haciendo la iteración sobre el arreglo
	unset($question['question']);
	// Obtiene las respuestas disponibles para la pregunta actual
	if (count($question)) {
		foreach ($question as $answer) {
			$cadena = strtr(strtolower($answer['answerText']), "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÜÚ","àáâãäåæçèéêëìíîïðñòóôõöøùüú");
			$cadena = ucwords(strtolower($cadena));
			$answers[$answer['answerText']] = $cadena;
		}
	} else {
		//Se le presenta al usuario el error de las preguntas
		drupal_get_messages();
		drupal_set_message(t('Ha ocurrido un error al conseguir las preguntas. Por favor intente validarse nuevamente.'), 'error');
		drupal_goto('case_military/authentication');
	}

	// Define el campo de identificación de la pregunta
	$form['id_question'] = array(
			'#type' => 'hidden',
			'#value' => $id_question,
	);

	// Define el campo de respuestas
	$form['selected_answer'] = array(
			'#type' => 'radios',
			'#title' => $question_text,
			'#required' => TRUE,
			'#options' => $answers,
	);
	// Submit para enviar el formulario
	$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Enviar'),
	);
	return $form;
}
//Funcion que valida las respuestas seleccionadas
function caz_validate_answer($identification, $id_question, $selected_answer) {
	//se almacenan las variables de url del webservice y la llave
	$url = variable_get('caz_siath_ws_url', '');
	$key = variable_get('caz_siath_ws_key', '');
	// Crea el cliente de soap
	$soapclient = new SoapClient($url);
	//Preparamos los parámetros
	$parametros = array(
			'identification' => $identification,
			'idQuestion' => $id_question,
			'selectedAnswer' => $selected_answer,
			'key' => $key,
	);
	try {
		//se validan las respuestas de los usuarios
		$result = $soapclient->__call('validateUserAnswer', $parametros);
	} catch (Exception $e) {
		//Se le informa a usuario el error presentado
		drupal_get_messages();
		drupal_set_message('Se ha presentado un error, por favor intentelo nuevamente (' . $e->getMessage() . ").\n", 'error');
		return;
	}
	return $result;
}
//Submit del formulario que valida las preguntas
function caz_identity_form_submit($form, &$form_state){
	//variable que valida que el usuario pase por la validación de usuario
	$_SESSION['caz']['flag']=FALSE;
	//almacena los valores de la pregunta
	$id_question = $form_state['values']['id_question'];
	//almacena los valores de las respuestas
	$selected_answer = $form_state['values']['selected_answer'];
	$questions_quantity = variable_get('caz_questions_quantity', '');
	//Guardamos en sesión las respuestas del usuario hasta que haya contestado todas las preguntas y en ese momento se valida la identidad
	if ($_SESSION["caz"]['step_index'] < $questions_quantity) {
		$_SESSION["caz"]["step"][$_SESSION["caz"]['step_index']]["id_question"] = $id_question;
		$_SESSION["caz"]["step"][$_SESSION["caz"]['step_index']]["answer"] = $selected_answer;
		$_SESSION["caz"]['step_index']++;
		$form_state['rebuild'] = true;
	} else if ($_SESSION["caz"]['step_index'] == $questions_quantity) {
		$_SESSION["caz"]["step"][$_SESSION["caz"]['step_index']]["id_question"] = $id_question;
		$_SESSION["caz"]["step"][$_SESSION["caz"]['step_index']]["answer"] = $selected_answer;
		foreach ($_SESSION["caz"]["step"] as $question) {
			//se valida que las respuestas sean correctas
			if (!caz_validate_answer($_SESSION["caz"]["identification"], $question["id_question"], $question["answer"])) {
				//si las preguntas no son correctas se le informa al usuario y se redirecciona a la autenticación
				drupal_set_message(t("No ha superado la validación de identidad."), "error");
				$form_state['redirect'] = "createAccountZimbra/authentication/siath";
				unset($_SESSION["caz"]);
				unset($form_state['values']);
				return;
			}
		}
		var_dump($_SESSION["caz"]["active"]);
		//exit();
		if($_SESSION["caz"]["mail"] != '' && $_SESSION["caz"]["active"] != 'SI'){
			drupal_set_message(t('Usted no se encuentra activo en el SIATH'));
			unset($form_state['values']);
			$form_state['redirect'] = "/user/login";
		}
		elseif($_SESSION["caz"]["mail"] != '' && $_SESSION["caz"]["active"] == 'SI'){
			//si todo es correcto  el usuario pasa a seleccionar el tipo de solicitud
			drupal_set_message(t('Usted ya tiene cuenta institucional en el SIATH'));
			unset($form_state['values']);
			$form_state['redirect'] = "/user/login";
		}
		elseif($_SESSION["caz"]["mail"] == '' && $_SESSION["caz"]["active"] == 'SI'){
			//si todo es correcto  el usuario pasa a seleccionar el tipo de solicitud
			drupal_set_message(t("Usted se ha validado correctamente, ahora puede diligenciar el formulario de solicitud de correo"));
			unset($form_state['values']);
			$_SESSION['caz']['flag']=TRUE;
			$form_state['redirect'] = "/node/add/cuenta-correo-institucional";
		}
	}
}
//Funcion que retorna las preguntas para cada usuario
function caz_get_questions($identification, $retired_person = 0) {
	//almacenamiento de la url del siath
	$url = variable_get('caz_siath_ws_url', '');
	//se almacena la llave de la conexion al siath
	$key = variable_get('caz_siath_ws_key', '');
	//se almacena la cantidad de preguntas y de respuestas
	$questions_quantity = variable_get('caz_questions_quantity', '');
	$answers_quantity = variable_get('caz_answers_quantity', '');
	// Crea el cliente de soap
	$soapclient = new SoapClient($url);
	try {
		// Pedimos las preguntas
		$resultado = $soapclient->getUserQuestions($identification, $questions_quantity, $answers_quantity, $retired_person, $key);
	} catch (Exception $e) {
		drupal_get_messages();
		drupal_set_message('Se ha presentado un error, por favor intentelo nuevamente (' . $e->getMessage() . ").\n", 'error');
		return;
	}
	//$preguntas
	return $resultado;
}