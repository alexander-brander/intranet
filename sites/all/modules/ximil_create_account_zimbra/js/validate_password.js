jQuery(document).ready(function(){	
	jQuery("#cuenta-correo-institucional-node-form").submit(function () {  
		//validar que tenga 6 caracteres
	    if(jQuery("#edit-field-password-caz-und-0-pass1").val().length < 6) { 
	    	jQuery( "#edit-field-password-caz" ).prepend( "<p class='message_paswd'>Debe ingresar por lo menos 6 caracteres</p>" );
	        return false;     
	    }else{
	    	jQuery('.message_paswd').remove();

	    }
	    //validar que tenga letras minusculas
	    if(jQuery("#edit-field-password-caz-und-0-pass1").val().match(/[a-z]/)) { 
	    	jQuery('.message_paswd').remove();  
	    }else{
	    	jQuery( "#edit-field-password-caz" ).prepend( "<p class='message_paswd'>Debe Añadir letras minúsculas</p>" );
	        return false; 
	    }
	    //validar letras mayusculas
	    if(jQuery("#edit-field-password-caz-und-0-pass1").val().match(/[A-Z]/)) { 
	    	jQuery('.message_paswd').remove();  
	    }else{
	    	jQuery( "#edit-field-password-caz" ).prepend( "<p class='message_paswd'>Debe Añadir letras mayúsculas</p>" );
	        return false; 
	    }
	    //validar que tenga cifras
	    if(jQuery("#edit-field-password-caz-und-0-pass1").val().match(/\d/)) { 
	    	jQuery('.message_paswd').remove();  
	    }else{
	    	jQuery( "#edit-field-password-caz" ).prepend( "<p class='message_paswd'>Debe Añadir cifras</p>" );
	        return false; 
	    }
	    //validar que tenga signos de puntuación
	    if(jQuery("#edit-field-password-caz-und-0-pass1").val().match(/\W/)) { 
	    	jQuery('.message_paswd').remove();  
	    }else{
	    	jQuery( "#edit-field-password-caz" ).prepend( "<p class='message_paswd'>Debe Añadir signos de puntuación</p>" );
	        return false; 
	    }
	});  
}); 