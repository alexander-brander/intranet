<?php

drupal_add_js(drupal_get_path('theme', 'armada_ximil') .'/iframe.js');
?>

  <div id="outer">
  <div class="bg-outer">
  <div class="dotted-bg">
	<div class="header-top">
		<div class="wrapper">
			<div class="header-section">
				   <div id="logo-floater">
        <?php if ($logo || $site_title): ?>
          <?php if ($title): ?>
            <div id="branding"><strong><a href="<?php print $front_page ?>">
            <?php if ($logo): ?>
              <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logo" />
            <?php endif; ?>
            <?php //print $site_html ?>
            </a></strong></div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="branding"><a href="<?php print $front_page ?>">
            <?php if ($logo): ?>
              <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logo" />
            <?php endif; ?>
            <?php //print $site_html ?>
            </a></h1>
        <?php endif; ?>
        <?php endif; ?>
        </div>
			</div>
			
		</div>
		
	</div>
	
	<div class="bottom-header-outer"> 
		<div class="bottom-header"> 
			<div class="wrapper">
			<?php print render($page['header']); ?>
			</div>
		</div>
	</div>
	
	<div class="slider">
		<div class="wrapper"><?php print render($page['slider']); ?></div>
		<div class="wrapper">
			<?php
			$array_days=array ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Fryday', 'Saturday', 'Sunday');
			$array_dias=array ('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
			$array_month=array ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
			$array_meses=array ('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiember', 'Octubre', 'Noviembre', 'Dicimbre');
			$fecha=date('l d \d\e F \d\e Y');
			$fecha=str_replace($array_days, $array_dias, $fecha);
			$fecha=str_replace($array_month, $array_meses, $fecha);
			echo "<span class='current-date'>$fecha<span>"
			?>
			<div class="tabber-container"><?php print render($page['tabbing_section']); ?></div></div>
	</div>
	</div>
	</div>
    <div id="container" class="clearfix">
		<div class="wrapper">
      <div id="center"><div id="squeeze"><div class="right-corner"><div class="left-corner">
          <?php print $breadcrumb; ?>
          <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
          <a id="main-content"></a>
          <?php if ($tabs): ?><div id="tabs-wrapper" class="clearfix"><?php endif; ?>
          <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <h1<?php print $tabs ? ' class="with-tabs"' : '' ?>><?php print $title ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php if ($tabs): ?><?php print render($tabs); ?></div><?php endif; ?>
          <?php print render($tabs2); ?>
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
      
            <?php print render($page['content']); ?>
         
          <?php //print $feed_icons ?>
          <?php //print render($page['footer']); ?>
      </div></div></div></div> <!-- /.left-corner, /.right-corner, /#squeeze, /#center -->
		</div>
		<div class="content-bottom-outer">
			<div class="wrapper">		
					 <?php print render($page['content_bottom']); ?>
			</div>
		</div>
    </div> <!-- /#container -->
	<div class="bg-bottom">
	<div class="content-accordian">
			<div class="wrapper">		
					 <?php print render($page['content_accordian']); ?>
			</div>
		</div>
	
	<div class="footer-outer">
		<div class="wrapper">		
			<div class="shadow">
				 <?php print render($page['footer']); ?>
			</div>	 
		</div>
	</div>
	</div>


	
  </div> <!-- /#outer -->
